import React from 'react';
import { mount, configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import {expect} from 'chai';
import {router} from 'react-router';

import Navigation from '../../src/components/Navigation';
configure({ adapter: new Adapter() })

describe('<Navigation/>', function () {
    const permissions = {
        'dashboard': true
    }
    const user = {
        isDemoCustomer: true
    }
    const company = 'test_company';
    const context = { router: { isActive: (a, b) => true } };

    it('should check a company prop', () => {
        const wrapper = shallow(
            <Navigation
                company={company}
                user={user}
                permissions={permissions}/>,
            { context }
            );
        expect(wrapper.props().company).to.be.defined;
    });

    it('should include header buttons', () => {
        const wrapper = shallow(
            <Navigation
                user={user}
                permissions={permissions}/>,
            { context }
        );
        expect(wrapper.find('.navbar-header')).to.have.length(1);
        expect(wrapper.find('.navbar-toggle')).to.have.length(1);
    });

    it('should include navigation links', () => {
        const wrapper = shallow(
            <Navigation
                user={user}
                permissions={permissions}/>,
            { context }
        );
        expect(wrapper.find('.sidebar')).to.have.length(1);
    });

    it('should not render links without company in properties', () => {
        const wrapper = shallow(
            <Navigation
                user={user}
                permissions={permissions}/>,
            { context }
        );
        expect(wrapper.find('#side-menu')).to.have.length(0);
    });
    it('should render links with a company as a property', () => {
        const wrapper = shallow(
            <Navigation
                company={company}
                user={user}
                permissions={permissions}/>,
            { context }
        );
        expect(wrapper.find('#side-menu')).to.have.length(1);
    });
    it('should render Dashboard for DemoCustomer', () => {
        const noDashboardermissions = {
            'dashboard': false
        }
        const wrapper = shallow(
            <Navigation
                company={company}
                user={user}
                permissions={noDashboardermissions}/>,
            { context }
        );
        expect(wrapper.find('.fa-line-chart')).to.have.length(1);
    });
    it('should not render Dashboard for non DemoCustomer', () => {
        const noDashboardermissions = {
            'dashboard': false
        }
        const wrapper = shallow(
            <Navigation
                company={company}
                user={{isDemoCustomer: false}}
                permissions={noDashboardermissions}/>,
            { context }
        );
        expect(wrapper.find('.fa-line-chart')).to.have.length(0);
    });

});
