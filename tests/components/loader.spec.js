import React from 'react';
import { mount,configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import {expect} from 'chai';

import Loader from '../../src/components/Loader';
configure({ adapter: new Adapter() })

describe('<Loader/>', function () {
    it('should have an image and layer for Loader', function () {
        const wrapper = shallow(<Loader/>);
        expect(wrapper.find('.b-loader__icon')).to.have.length(1);
        expect(wrapper.find('.b-loader__layer')).to.have.length(1);
    });
});
