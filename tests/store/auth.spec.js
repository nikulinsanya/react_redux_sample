//https://redux.js.org/recipes/writing-tests
import * as reducer from '../../src/store/auth/reducer';
import {LOGIN_SUCCESS} from  '../../src/store/auth/constants';

const initialState = {
    company: 'test_company',
    token: null,
    userName: null,
    loginError: null,
    isPasswordReset: null
};

const authReducer = reducer.default;
describe('auth reducer', () => {

    it('should return the initial state', () => {
        expect(authReducer(initialState, {})).to.equal(initialState);
    });

    it('should return token on LOGIN_SUCESS', () => {
        const token = 'TESTTOKEN';

        expect(authReducer(
            initialState,
            {
                type: LOGIN_SUCCESS,
                token
            }))
        .to.deep.include({token});
    })
});
