import React, { Component } from 'react';
import { Link } from 'react-router';
import PropTypes from 'prop-types';
import cx from 'classnames';
import {getConfig} from 'constants';

class Navigation extends Component {
    constructor(props) {
        super(props);
        this.state = {
            openSidebar: false,
            openCharts: false,
            openUserMenu: false,
            openAddressesMenu: false,
            selectedAddress: null
        };
        this.toggleNav = this.toggleNav.bind(this);
        this.logout = this.logout.bind(this);
        this.toggleInCharts = this.toggleInCharts.bind(this);
        this.toggleUserMenu = this.toggleUserMenu.bind(this);
        this.toggleAddressesMenu = this.toggleAddressesMenu.bind(this);
        this.selectAddress = this.selectAddress.bind(this);
    }

    toggleNav() {
        this.setState({
            openSidebar: !this.state.openSidebar,
            openUserMenu: false,
            openAddressesMenu: false
        });
    }
    toggleInCharts() {
        this.setState({
            openCharts: !this.state.openCharts,
            openUserMenu: false,
            openAddressesMenu: false
        });
    }
    toggleUserMenu() {
        this.setState({
            openUserMenu: !this.state.openUserMenu,
            openAddressesMenu: false
        });
    }
    toggleAddressesMenu() {
        this.setState({
            openAddressesMenu: !this.state.openAddressesMenu,
            openUserMenu: false
        });
    }
    logout() {
        this.props.logout();
        this.context.router.push(`/login`)
    }
    selectAddress(address) {
        this.toggleAddressesMenu();
        this.props.selectAddress(address);
    }
    renderAddresses() {
        const addresses = this.props.user && this.props.user.addresses;
        const self = this;

        if (!addresses) {
            return <li><a onClick={() => { self.selectAddress(); this.toggleAddressesMenu(); }}>
                &nbsp;No addresses&nbsp;</a></li>
        }
        const list = addresses.map((item) => {
            return <li key={item.id}>
                <a onClick={() => { self.selectAddress(item) }}>
                    {(item.title ? (item.title + ': ') : '') +
                    item.street + ' ' + item.buildingNr + item.buildingNrEx + ', ' + item.city}
                </a>
            </li>
        })

        const isDemoCustomer = this.props.user && this.props.user.isDemoCustomer;

        if (!isDemoCustomer) {
            list.push(<li key="newa"><a href="#/profile/addaddresses" onClick={this.toggleAddressesMenu}>
                <i className="fa fa-plus fa-fw"/> Voeg locatie toe</a>
            </li>);
        }
        return list;
    }

    renderMenuItems() {
        const isDemoCustomer = this.props.user && this.props.user.isDemoCustomer;
        if (isDemoCustomer) {
            return (
                <ul className="dropdown-menu dropdown-user">
                    <li><a onClick={this.logout}><i className="fa fa-sign-out fa-fw" /> Logout</a></li>
                </ul>
            );
        }
        return (
                <ul className="dropdown-menu dropdown-user">
                <li><a href="#/profile/addresses" onClick={this.toggleUserMenu}>
                    <i className="fa fa-home fa-fw" /> Beheer locaties</a>
                </li>
                <li><a href="#/profile/personal" onClick={this.toggleUserMenu}>
                    <i className="fa fa-user fa-fw" /> Mijn profiel</a>
                </li>
                <li className="divider" />
                    <li><a onClick={this.logout}><i className="fa fa-sign-out fa-fw" /> Logout</a></li>
                </ul>
        )
    }
    renderUserMenu() {
        const toggleUserMenuClass = cx({
            'open': this.state.openUserMenu,
            'dropdown': true
        });
        return (
            <li className={toggleUserMenuClass}>
                <a className="dropdown-toggle" data-toggle="dropdown" onClick={this.toggleUserMenu}>
                    <i className="fa fa-user fa-fw" /> <i className="fa fa-caret-down" />
                </a>
                {this.renderMenuItems()}
            </li>
        )
    }
    renderLinks() {
        const {company, user, permissions} = this.props;
        const isDemoCustomer = user && user.isDemoCustomer;

        if (!company) {
            return null;
        }

        const toggleChartsClass = cx({
            'in': this.state.openCharts,
            'nav nav-second-level': true
        });
        const chartsSideClass = cx({
            'active': this.state.openCharts
        });
        return (
            <ul className="nav" id="side-menu">
                {
                    isDemoCustomer ||
                    (permissions['dashboard'])
                    ? <li>
                        <Link to={`/dashboard`} activeClassName="active" onClick={this.toggleNav}>
                            <i className="fa fa-line-chart fa-fw"/> Dashboard</Link>
                    </li>
                    : null
                }
                {
                    isDemoCustomer ||
                    (permissions['electricity'])
                    ? <li>
                        <Link to={`/electricity`} activeClassName="active" onClick={this.toggleNav}>
                            <i className="fa fa-flash fa-fw" /> Elektriciteit </Link>
                    </li>
                    : null
                }
            </ul>
        )
    }

    renderLogo() {
        const {company, user} = this.props;
        const adviserConfig = getConfig(user && user.adviserCode);
        const isDemoCustomer = user && user.isDemoCustomer;
        const poweredBy = null;// <span className="powered-by">Powered by SmartDodos</span>;
        const showPowerBy = !!adviserConfig && adviserConfig.permissions && adviserConfig.permissions.showPowerBy;

        const navbarClass = cx({
            'with-adviser': showPowerBy,
            'navbar-brand': true
        });

        return (
            <span className={navbarClass}>
                {showPowerBy ? poweredBy : null}
                {isDemoCustomer && !showPowerBy ? <a className="demo-link" href="https://www.smartdodos.com/"> Demo </a> : null}
            </span>
        )
    }

    isPrivate(address) {
        return address
        ? ['Appartement', 'Row', 'Corner', 'Semidetached', 'Detached'].indexOf(address.buildingType) !== -1
        : true;
    }

    render() {
        const toggleNavClass = cx({
            'in': this.state.openSidebar,
            'sidebar-nav navbar-collapse': true
        });

        const toggleAddressesMenuClass = cx({
            'open': this.state.openAddressesMenu,
            'dropdown': true
        });
        const { selectedAddress } = this.props;

        let addressTitle = selectedAddress &&
            ((selectedAddress.title ? (' ' +
                selectedAddress.title + ': ' +
                selectedAddress.street + ' ' +
                selectedAddress.buildingNr +
                (selectedAddress.buildingNrEx ? selectedAddress.buildingNrEx : '') +
                ', ' + selectedAddress.city +
                ' ') : false) ||
                (' ' + selectedAddress.street + ' ' + selectedAddress.buildingNr +
                    (selectedAddress.buildingNrEx ? selectedAddress.buildingNrEx : '') + ' ')) ||
            ' Add location ';

        const isPrivate = this.isPrivate(selectedAddress);
        const iconCLass = cx({
            'fa': true,
            'fa-home': isPrivate,
            'fa-shopping-basket': !isPrivate
        });

        return (
            <nav className="navbar navbar-default navbar-static-top" role="navigation">
                <div className="navbar-header">
                    <button type="button" className="navbar-toggle" onClick={this.toggleNav}>
                        <span className="sr-only">Toggle navigation</span>
                        <span className="icon-bar" />
                        <span className="icon-bar" />
                        <span className="icon-bar" />
                    </button>
                    {this.renderLogo()}
                </div>

                <ul className="nav navbar-top-links navbar-right">
                    <li className={toggleAddressesMenuClass}>
                        {selectedAddress
                            ? <a className="dropdown-toggle" data-toggle="dropdown" onClick={this.toggleAddressesMenu}>
                                <i className={iconCLass}/>
                                {addressTitle}
                                <i className="fa fa-caret-down"/>
                            </a>
                            : <a href="#/profile/addaddress"> <i className="fa fa-home"/> Voeg locatie toe </a>
                        }
                        <ul className="dropdown-menu dropdown-user">
                            {this.renderAddresses()}
                        </ul>

                    </li>
                    {this.renderUserMenu()}
                </ul>

                <div className="navbar-default sidebar" role="navigation">
                    <div className={toggleNavClass}>
                        {this.renderLinks()}
                    </div>
                </div>
            </nav>
        )
    }
}
Navigation.contextTypes = {
    router: PropTypes.object.isRequired
};
Navigation.propTypes = {
    logout: PropTypes.func,
    selectAddress: PropTypes.func,
    selectedAddress: PropTypes.object,
    user: PropTypes.object,
    permissions: PropTypes.object,
    company: PropTypes.string
};

export default Navigation;
