import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Notification extends Component {
    renderCloseIcon() {
        const {hideNotification, showCloseIcon, index} = this.props;

        if (!showCloseIcon) {
            return null;
        }
        window.scrollTo(0, 0);
        return (
            <i className="fa fa-times b-notification__close-icon"
               onClick={() => hideNotification(index)}
               aria-hidden="true"/>
        );
    }

    render() {
        const {type, message} = this.props;

        return (
            <div className={`b-notification b-notification--${type}`}>
                <div className="b-notification__message">
                    <span>{message}</span>
                </div>
                {this.renderCloseIcon()}
            </div>
        );
    }
}

Notification.propTypes = {
    type: PropTypes.oneOf(['success', 'error', 'info']).isRequired,
    message: PropTypes.string.isRequired,
    showCloseIcon: PropTypes.bool,
    hideNotification: PropTypes.func.isRequired,
    index: PropTypes.number.isRequired
};

Notification.defaultProps = {
    showCloseIcon: true
};

export default Notification;
