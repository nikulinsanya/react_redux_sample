import React, { Component } from 'react';
class WidgetPreloader extends Component {
    render() {
        return (
                <div className="preloader">
                    <i className="fa fa-spinner fa-pulse fa-3x fa-fw" />
                    <span className="sr-only">Loading...</span>
                </div>
        )
    }
}

export default WidgetPreloader;
