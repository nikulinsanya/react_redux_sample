import React, { Component } from 'react';
import PropTypes from 'prop-types';
import DateRangePicker from 'react-bootstrap-daterangepicker';
import { DISPLAY_DATE_FORMATS, COMPANY_CONFIGS } from 'constants';
import moment from 'moment';
import cx from 'classnames';

class DateRangePickerWrapper extends Component {
    constructor(props) {
        super(props);
        this.ranges = Object.assign({}, props.range, {
            'Gisteren': [moment().subtract(1, 'days').startOf('day'), moment().subtract(1, 'days').endOf('day')],
            'Afgelopen 7 dagen': [moment().subtract(7, 'days'), moment()],
            'Afgelopen 30 dagen': [moment().subtract(30, 'days'), moment()],
            'Afgelopen 90 dagen': [moment().subtract(90, 'days'), moment()],
            'Vorig jaar': [moment().subtract(1, 'years'), moment()]});
        this.onDateChange = this.onDateChange.bind(this);
    }

    onDateChange(event, picker) {
        const {startDate, endDate} = picker;
        const {from, to} = this.props;
        const {onDateRangeChange} = this.props;

        if (startDate.isSame(from, 'day') && endDate.isSame(to, 'day')) {
            return;
        }

        onDateRangeChange(startDate, endDate);
    }

    render() {
        const {from, to, tabIndex, dateFormat, label, maxDate} = this.props;
        const now = maxDate || moment().add(-1, 'day').startOf('day');

        const labelItem = label ? null : <label className="b-filters__label">{label}</label>;

        return <div className="b-filters__item b-filters__item--date-filter b-date-filter">
                {labelItem}
            <DateRangePicker
                maxDate={now}
                startDate={from}
                endDate={to}
                ranges={this.ranges}
                weekStart="0"
                onApply={this.onDateChange}
                onHide={this.onDateChange}
                parentEl=".b-wrapper"
                locale={{
                    format: dateFormat || DISPLAY_DATE_FORMATS.DATE_PICKER,
                    'separator': ' - ',
                    'applyLabel': 'Pas toe',
                    'cancelLabel': 'Cancel',
                    'fromLabel': 'Van',
                    'toLabel': 'naar',
                    'customRangeLabel': 'Aangepast',
                    'daysOfWeek': [
                        'Mo',
                        'Tu',
                        'We',
                        'Th',
                        'Fr',
                        'Sa',
                        'Su'
                    ],
                    'monthNames': [
                        'januari',
                        'februari',
                        'maart',
                        'april',
                        'mei',
                        'juni',
                        'juli',
                        'augustus',
                        'september',
                        'oktober',
                        'november',
                        'december'
                    ]
                }}
            >
                <button
                    className="b-date-filter__button"
                    tabIndex={tabIndex}>
                    <i className="fa fa-calendar b-date-filter__calendar-icon"/>
                    <div>
                        <span>{from.format(dateFormat || DISPLAY_DATE_FORMATS.DATE_PICKER)} </span>
                        -
                        <span> {to.format(dateFormat || DISPLAY_DATE_FORMATS.DATE_PICKER)}</span>
                    </div>
                </button>
            </DateRangePicker>
        </div>;
    }
}

DateRangePickerWrapper.propTypes = {
    from: PropTypes.object.isRequired,
    to: PropTypes.object.isRequired,
    maxDate: PropTypes.object,
    onDateRangeChange: PropTypes.func.isRequired,
    tabIndex: PropTypes.number.isRequired,
    dateFormat: PropTypes.string,
    label: PropTypes.bool,
    range: PropTypes.object
};

export default DateRangePickerWrapper;
