import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Notification from 'components/Notification';

class NotificationsHolder extends Component {
    render() {
        const {notifications, hideNotification} = this.props;
        if (!notifications) {
            return null;
        }

        return (
            <div className="b-notifications-holder">
                {notifications.map((notification, index) => <Notification
                    key={index}
                    index={index}
                    hideNotification={hideNotification} {...notification}/>)}
            </div>
        );
    }
}

NotificationsHolder.propTypes = {
    notifications: PropTypes.array,
    hideNotification: PropTypes.func.isRequired
};

export default NotificationsHolder;
