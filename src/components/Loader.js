import React from 'react';

const Loader = () => {
    return (
        <div className="b-loader">
            <div className="b-loader__layer"/>
            <div className="b-loader__icon"/>
        </div>
    );
};

export default Loader;
