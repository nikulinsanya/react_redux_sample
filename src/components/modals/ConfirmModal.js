import React, { Component } from 'react';
import Modal from 'react-modal';
import PropTypes from 'prop-types';

class ConfirmModal extends Component {
    constructor(props) {
        super(props);
        this.onConfirm = this.onConfirm.bind(this);
    }

    onConfirm() {
        const {confirmCallback, closeModal} = this.props;
        closeModal();
        confirmCallback();
    }

    render() {
        const {
            message,
            isOpen,
            closeModal,
            confirmButtonText,
            cancelButtonText
        } = this.props;
        const customStyles = {
            content: {
                'height': '200px',
                'width': '350px',
                'margin': 'auto',
                'borderRadius': 0,
                'border': 'none',
                'boxShadow': '0px 2px 20px rgba(0, 0, 0, 0.15)'
            }
        };

        return (
            <Modal
                style={customStyles}
                shouldCloseOnOverlayClick
                isOpen={isOpen}
                onRequestClose={closeModal}
                contentLabel="Confirm"
            >
                <div className="b-modal">
                    <i className="fa fa-times b-modal__close" aria-hidden="true" onClick={closeModal}/>
                    <div className="b-modal__message">
                        {message}
                    </div>
                    <div className="b-modal__buttons">
                        <button className="b-button b-button--modal b-button--cancel" onClick={closeModal}>
                            {cancelButtonText}
                        </button>
                        <button className="b-button b-button--modal"
                                onClick={this.onConfirm}>
                            {confirmButtonText}
                        </button>
                    </div>
                </div>
            </Modal>
        );
    }
}

ConfirmModal.propTypes = {
    message: PropTypes.string.isRequired,
    isOpen: PropTypes.bool.isRequired,
    closeModal: PropTypes.func.isRequired,
    confirmCallback: PropTypes.func.isRequired,
    confirmButtonText: PropTypes.string,
    cancelButtonText: PropTypes.string
};

ConfirmModal.defaultProps = {
    cancelButtonText: 'No',
    confirmButtonText: 'Yes'
};

export default ConfirmModal;
