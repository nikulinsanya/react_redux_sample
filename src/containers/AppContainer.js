import React, { Component } from 'react';
import { Router } from 'react-router';
import { Provider } from 'react-redux';
import PropTypes from 'prop-types';

class AppContainer extends Component {
    static propTypes = {
        routes: PropTypes.object.isRequired,
        store: PropTypes.object.isRequired,
        history: PropTypes.object.isRequired
    };

    render() {
        const {routes, store, history} = this.props;

        return (
            <Provider store={store}>
                <div className="b-app">
                    <Router history={history} children={routes}/>
                </div>
            </Provider>
        );
    }
}

export default AppContainer;
