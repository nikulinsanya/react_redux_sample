import _ from 'lodash';
var authToken = '';
import moment from 'moment';

function fetchAndFilterResponse(url, options, skipAuth, useBlob) {
    options = options || {method: 'GET'};
    if (options && (options.method === 'POST' || options.method === 'PUT')) {
        if (options.body && _.isObject(options.body)) {
            options.body = JSON.stringify(options.body);
        }

        options.headers = Object.assign({}, {
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        }, options.headers);
    }

    if (!skipAuth) {
        const token = authToken;
        options.headers = Object.assign({}, options.headers, {
            Authorization: `Bearer ${token}`
        });
    }

    return fetch(url, options)
        .then(response => {
            if (response.status >= 200 && response.status < 300) {
                return response.text();
            }

            if (response.status === 401) {
                throw new Error(401);
            }

            if (response.status === 403) {
                throw new Error(403);
            }

            return response.json().then(Promise.reject.bind(Promise));
        })
        .then(text => text.length ? JSON.parse(text) : {});
}


class APIService {
    static setToken(token) {
        authToken = token;
    };

    static getToken(loginData) {
        let data = `grant_type=password&userName=${
            loginData.userName
        }&password=${
            loginData.password
        }`;
        if (loginData.useRefreshTokens) {
            data = data + '&client_id=ngAuthApp';
        }
        return fetchAndFilterResponse(
            `${__AUTH_URL__}token`,
            {
                method: 'POST',
                body: data,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            },
            true
        );
    }

    static getCurrentUser(inclStopped) {
        return fetchAndFilterResponse(`${__API_URL__}api/customer${inclStopped ? '?showStopped=true' : ''}`);
    }

    static getP4Usage(filter) {
        const dates = filter.from === filter.to
            ? filter.from
            : `${filter.from}/${filter.to}`;
        return fetchAndFilterResponse(
            `${__API_URL__}api/P4/${filter.ean}/${dates}?includePrevData=${!!filter.includePrev}`
        );
    }
    static getMaxKwH(filter) {
        const dates = `${filter.from}/${filter.to}`;
        return fetchAndFilterResponse(
            `https://p42.umeter.nl/api/usageMax/${filter.ean}/${dates}`
        );
    }
}

export default APIService;
