export const PERMISSIONS = {
    READ_ONLY: 1,
    READ_WRITE: 2
};

export const COMPONENTS = {
    DASHBOARD: 1,
    ELECTRICITY: 2,
    PROFILE: 3
};

export const DATE_FORMATS = {
    MONTH_DAY_YEAR: 'MM / DD / YYYY',
    DAY_MONTH_YEAR: 'DD / MM / YYYY',
    YEAR_MONTH_DAY: 'YYYY / MM / DD',
    YEAR_DAY_MONTH: 'YYYY / DD / MM'
};

export const DISPLAY_DATE_FORMATS = {
    LONG: 'MM/DD/YYYY hh:mm:ss A',
    DATE_PICKER: 'DD MMM YYYY'
};

export const CHART_COLORS = [
    '#86af3f',
    '#93c43e',
    '#a6e539',
    '#affc2a',
    '#79af1a',
    '#6ca30d',
    '#578704',
    '#436803',
    '#fff34f',
    '#f4e622',
    '#ffee00',
    '#ffdb0f'
];

export const COMPANY_CONFIGS = {
    SMARTDODOS:{
        mainClass: 'dodos',
        name: 'Smartdodos',
        logo: 'dodos_logo.png',
        favicon: 'https://app.smartdodos.com/favicon.ico',
        showSocialButtons: false,
        fb_api_key: 'XXX',
        google_api_key: {
            clientId: 'XX',
            apiKey: 'XX'
        },
        permissions: {
            showPowerBy: false
        },
        demoCredentials:{
            'email': 'demo@demo',
            'password': 'demodemo'
        }
    }
}

// currentUser/reducer - to temporary hack adviserCode
export function getConfig(name) {
    return COMPANY_CONFIGS[name] || COMPANY_CONFIGS['SMARTDODOS']
}

export function getAddAddressCheckboxText(name) {
    const labels = {
        'SMARTDODOS': 'Ik geef SmartDodos en haar partner AmperaEnergie toestemming om op veilige wijze meterstanden te verzamelen zonder deze aan derden teverstreken zonder verdere toestemming'
    }

    return labels[name] || labels['SMARTDODOS']
}
