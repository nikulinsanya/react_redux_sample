export function getErrorMessage(error, messageIfNoErrorFromBackend) {
    if (error && error.Errors) {
        return Object.values(error.Errors).join(' ');
    } else if (messageIfNoErrorFromBackend) {
        return messageIfNoErrorFromBackend;
    }
}
