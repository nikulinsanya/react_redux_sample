import { logout } from 'store/auth/actions';
import { push } from 'react-router-redux';

const accessMiddleware = store => next => action => {
    try {
        const {error = {}} = action;
        const {message} = error;
        if (message === '401') {
            store.dispatch(logout());
            store.dispatch(push('/login'));
        } else if (message === '403') {
            store.dispatch(push('/no-access'));
        }
        return next(action);
    } catch (err) {
        throw err;
    }
};

export default accessMiddleware;
