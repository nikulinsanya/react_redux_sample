import {
    GET_ELECTRICITY_DATA_REQUEST,
    GET_ELECTRICITY_DATA_FAILURE,
    GET_ELECTRICITY_DATA_SUCCESS,
    GET_MAX_KWH_REQUEST,
    GET_MAX_KWH_FAILURE,
    GET_MAX_KWH_SUCCESS
} from './constants';
import APIService from 'services/APIService';
import _ from 'lodash';

import { getErrorMessage } from 'helpers/errors';

export function getP4Usage(filter) {
    return (dispatch, getStore) => {
        dispatch({
            type: GET_ELECTRICITY_DATA_REQUEST
        });

        const address = getStore().currentUser.selectedAddress;
        if (!address || !address.eEan || !address.eEan.ean) {
            dispatch({
                type: GET_ELECTRICITY_DATA_FAILURE
            });
            return;
        }
        const newFilter = _.assign({...filter}, {ean: address.eEan.ean});

        APIService.getP4Usage(newFilter).then(data => {
            dispatch({
                type: GET_ELECTRICITY_DATA_SUCCESS,
                data,
                filter: filter
            });
        }, error => {
            dispatch({
                type: GET_ELECTRICITY_DATA_FAILURE,
                error
            });
        });
    };
}

export function getMaxKwh(filter) {
    return (dispatch, getStore) => {
        dispatch({
            type: GET_MAX_KWH_REQUEST
        });

        const address = getStore().currentUser.selectedAddress;
        if (!address || !address.eEan || !address.eEan.ean) {
            dispatch({
                type: GET_MAX_KWH_FAILURE
            });
            return;
        }
        const newFilter = _.assign({...filter}, {ean: address.eEan.ean});

        APIService.getMaxKwH(newFilter).then(data => {
            dispatch({
                type: GET_MAX_KWH_SUCCESS,
                data,
                filter: filter
            });
        }, error => {
            dispatch({
                type: GET_MAX_KWH_FAILURE,
                error
            });
        });
    };
}
