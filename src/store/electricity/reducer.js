import {
    GET_ELECTRICITY_DATA_SUCCESS,
    GET_ELECTRICITY_DATA_FAILURE,
    GET_MAX_KWH_FAILURE,
    GET_MAX_KWH_SUCCESS
} from './constants';

const ACTION_HANDLERS = {
    [GET_ELECTRICITY_DATA_SUCCESS]: (state, action) => {
        const {data} = action;
        return {
            ...state,
            consumption: data
        };
    },
    [GET_ELECTRICITY_DATA_FAILURE]: (state, action) => {
        const {data} = action;
        return {
            ...state,
            consumption: {}
        };
    },
    [GET_MAX_KWH_SUCCESS]: (state, action) => {
        const {data} = action;
        return {
            ...state,
            maxKwh: data
        };
    },
    [GET_MAX_KWH_FAILURE]: (state, action) => {
        const {data} = action;
        return {
            ...state,
            maxKwh: null
        };
    }
};

const getInitialState = () => {
    return {
        consumption: null,
        maxKwh: null
    };
};

const initialState = getInitialState();

export default function electricityReducer(state = initialState, action) {
    const {type} = action;
    const handler = ACTION_HANDLERS[type];
    return handler ? handler(state, action) : state;
};
