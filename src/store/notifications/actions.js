import {
    SHOW_NOTIFICATION,
    HIDE_NOTIFICATION,
    HIDE_ALL_NOTIFICATIONS
} from './constants';
import APIService from 'services/APIService';

export function showNotification(notification) {
    return (dispatch) => {
        dispatch({
            type: SHOW_NOTIFICATION,
            notification
        });
    };
}

export function hideNotification(index) {
    return (dispatch) => {
        dispatch({
            type: HIDE_NOTIFICATION,
            index
        });
    };
}

export function hideAllNotifications() {
    return (dispatch) => {
        dispatch({
            type: HIDE_ALL_NOTIFICATIONS
        });
    };
}

export function updateNotifications(data) {
    return (dispatch, getStore) => {
        APIService.updateNotifications(data).then(data => {
            showNotification('Updated');
        }, error => {
        });
    };
}
