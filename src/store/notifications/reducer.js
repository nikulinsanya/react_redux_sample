import {
    SHOW_NOTIFICATION,
    HIDE_NOTIFICATION,
    HIDE_ALL_NOTIFICATIONS
} from './constants';
import _ from 'lodash';

const ACTION_HANDLERS = {
    [SHOW_NOTIFICATION]: (state, action) => {
        const {notification} = action;

        return {
            ...state,
            notifications: [..._.takeRight(state.notifications, 2), notification]
        };
    },
    [HIDE_NOTIFICATION]: (state, action) => {
        const {index} = action;

        return {
            ...state,
            notifications: [...state.notifications.slice(0, index), ...state.notifications.slice(index + 1)]
        };
    },
    [HIDE_ALL_NOTIFICATIONS]: (state) => {
        return {
            ...state,
            notifications: []
        };
    }
};

const initialState = {
    notifications: []
};

export default function notificationsReducer(state = initialState, action) {
    const {type} = action;
    const handler = ACTION_HANDLERS[type];
    return handler ? handler(state, action) : state;
};
