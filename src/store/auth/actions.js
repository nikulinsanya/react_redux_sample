import {
    LOGIN_FAILURE,
    LOGIN_REQUEST,
    LOGIN_SUCCESS,
    CLEAR_LOGIN_ERROR,
} from './constants';

import APIService from 'services/APIService';
import { push } from 'react-router-redux';

export function getToken(loginData) {
    return (dispatch) => {
        dispatch({
            type: LOGIN_REQUEST
        });

        APIService.getToken(loginData)
            .then(json => {
                const {access_token} = json;
                APIService.setToken(access_token);

                dispatch({
                    type: LOGIN_SUCCESS,
                    token: access_token
                });

                dispatch(push('/'));
            }, error => {
                dispatch({
                    type: LOGIN_FAILURE,
                    error
                });
            });
    };
}


export function logout() {
    return (dispatch) => {
        dispatch({
            type: LOGIN_SUCCESS,
            token: null
        });
    };
}

export function clearLoginError() {
    return dispatch => {
        dispatch({
            type: CLEAR_LOGIN_ERROR
        });
    };
}
