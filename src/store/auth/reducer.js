import {
    LOGIN_SUCCESS,
    LOGIN_FAILURE,
    LOGIN_REQUEST,
    CLEAR_LOGIN_ERROR,
    INIT_COMPANY_NAME
} from './constants';

const ACTION_HANDLERS = {
    [LOGIN_REQUEST]: (state) => {
        return {
            ...state,
            loginError: null
        };
    },
    [LOGIN_SUCCESS]: (state, action) => {
        const {token} = action;
        return {
            ...state,
            token
        };
    },
    [LOGIN_FAILURE]: (state, action) => {
        const {error = {}} = action;
        const {message} = error;
        let loginError;

        if (message === '401') {
            loginError = 'The user name or password is incorrect.';
        } else if (message === 'Failed to fetch') {
            loginError = 'Count not connect to the server. Might be connected to CORS problem.';
        } else {
            loginError = error.error_description || 'The user name or password is incorrect.';
        }

        return {
            ...state,
            loginError
        };
    },
    [CLEAR_LOGIN_ERROR]: state => {
        return {
            ...state,
            loginError: null
        };
    },
    [INIT_COMPANY_NAME]: state => {
        return {
            ...state,
            company: getCompanyName()
        };
    }
};

const getCompanyName = () => {
    const urls = [
        ['localhost', 'SMARTDODOS']
    ];
    for (let i in urls) {
        if (window.location.href.indexOf(urls[i][0]) !== -1) {
            return urls[i][1];
        }
    }
    return 'SMARTDODOS';
};

const initialState = {
    company: getCompanyName(),
    token: null,
    userName: null,
    loginError: null,
    isPasswordReset: null
};

export default function authReducer(state = initialState, action) {
    const {type} = action;
    const handler = ACTION_HANDLERS[type];

    return handler ? handler(state, action) : state;
};
