import {
    GET_CURRENT_USER_PROFILE_SUCCESS,
    GET_CURRENT_USER_PROFILE_FAILURE,
    GET_CURRENT_USER_PROFILE_REQUEST,
    GET_USER_PROFILE_INCL_STOPPED_SUCCESS,
    GET_USER_PROFILE_INCL_STOPPED_FAILURE,
    UPDATE_METER_REQUEST,
    UPDATE_METER_SUCCESS,
    UPDATE_METER_FAILURE,
    SELECT_ADDRESS,
    CLEAR_USER_STATE,
    SET_USER_PERMISSIONS,
    GET_P1_SUCCESS,
    GET_P1_FAILURE,
    UPDATE_MOVED_SUCCESS
} from './constants';

const ACTION_HANDLERS = {
    [GET_CURRENT_USER_PROFILE_SUCCESS]: (state, action) => {
        const {user} = action;
        return {
            ...state,
            user,
            isFetchingCurrentUserProfile: false
        };
    },
    [GET_CURRENT_USER_PROFILE_FAILURE]: state => {
        return {
            ...state,
            isFetchingCurrentUserProfile: false
        };
    },
    [GET_CURRENT_USER_PROFILE_REQUEST]: state => {
        return {
            ...state,
            isFetchingCurrentUserProfile: true
        };
    },
    [SELECT_ADDRESS]: (state, action) => {
        const {address} = action;
        return {
            ...state,
            selectedAddress: address
        };
    },
    [SET_USER_PERMISSIONS]: (state, action) => {
        const {user} = action;

        const hasGas = user.addresses && user.addresses.find((address) => {
            return address.gEan
        })
        const hasEl = user.addresses && user.addresses.find((address) => {
            return address.eEan
        })
        const permissions = {
            dashboard: user.addresses && user.addresses.length,
            electricity: user.addresses && user.addresses.length && !!hasEl
        }
        return {
            ...state,
            permissions
        };
    },
    [CLEAR_USER_STATE]: (state) => {
        return getInitialState();
    }
};

const getInitialState = () => {
    return {
        user: null,
        selectedAddress: null,
        isFetchingCurrentUserProfile: true,
        allAddresses: null,
        permissions: {
            dashboard: true,
            electricity: false
        }
    };
};

const initialState = getInitialState();

export default function reducer(state = initialState, action) {
    const {type} = action;
    const handler = ACTION_HANDLERS[type];

    return handler ? handler(state, action) : state;
};
