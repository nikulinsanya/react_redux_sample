import {
    GET_CURRENT_USER_PROFILE_FAILURE,
    GET_CURRENT_USER_PROFILE_REQUEST,
    GET_CURRENT_USER_PROFILE_SUCCESS,
    SELECT_ADDRESS,
    SET_USER_PERMISSIONS,
    CLEAR_USER_STATE
} from './constants';
import {
    logout
} from 'store/auth/actions';
import { push } from 'react-router-redux';
import APIService from 'services/APIService';

export function getUserProfile() {
    return dispatch => {
        dispatch({
            type: GET_CURRENT_USER_PROFILE_REQUEST
        });

        APIService.getCurrentUser().then((user) => {
            dispatch({
                type: GET_CURRENT_USER_PROFILE_SUCCESS,
                user
            });
            if (user.addresses && user.addresses.length) {
                dispatch({
                    type: SELECT_ADDRESS,
                    address:user.addresses[0]
                });
            }
            dispatch({
                type: SET_USER_PERMISSIONS,
                user:user
            });

        }, error => {
            dispatch(logout());
            dispatch(push('/login'));
            dispatch({
                type: GET_CURRENT_USER_PROFILE_FAILURE,
                error
            });
        });
    };
}

export function selectAddress(address) {
    return dispatch => {
        dispatch({
            type: SELECT_ADDRESS,
            address
        });
    }
}
export function clearUserState() {
    return dispatch => {
        dispatch({
            type: CLEAR_USER_STATE
        });
    }
}

