import {
    EL_DASHBOARD_DATA_REQUEST,
    EL_DASHBOARD_DATA_SUCCESS,
    EL_DASHBOARD_DATA_FAILURE,
    GAS_DASHBOARD_DATA_REQUEST,
    GAS_DASHBOARD_DATA_SUCCESS,
    GAS_DASHBOARD_DATA_FAILURE,
    GET_DASHBOARD_RT_DAY_DATA_REQUEST,
    GET_DASHBOARD_RT_DAY_DATA_SUCCESS,
    GET_DASHBOARD_RT_DAY_DATA_FAILURE,
    CLEAR_CUSTOMERS_STATE
} from './constants';
import _ from 'lodash';
import moment from 'moment';
import APIService from 'services/APIService';

import { getErrorMessage } from 'helpers/errors';

export function getElLasDay(ean, isDemo, from) {
    return (dispatch, getStore) => {
        dispatch({
            type: EL_DASHBOARD_DATA_REQUEST
        });

        const address = getStore().currentUser.selectedAddress;
        if (!ean && (!address || !address.eEan || !address.eEan.ean)) {
            dispatch({
                type: EL_DASHBOARD_DATA_FAILURE
            });
            return;
        }
        ean = ean || (address || address.eEan || address.eEan.ean);

        const newFilter = {
            ean: address.eEan.ean || ean,
            from:  from || moment().add(isDemo ? -2 : -1, 'day').format('YYYYMMDD'),
            to: from || moment().add(isDemo ? -2 : -1, 'day').format('YYYYMMDD'),
            includePrev: true};

        APIService.getP4Usage(newFilter).then(data => {
            dispatch({
                type: EL_DASHBOARD_DATA_SUCCESS,
                data
            });
        }, error => {
            dispatch({
                type: EL_DASHBOARD_DATA_FAILURE,
                error
            });
        });
    };
}

export function getGasLasDay(ean, isDemo, from) {
    return (dispatch, getStore) => {
        dispatch({
            type: GAS_DASHBOARD_DATA_REQUEST
        });

        const address = getStore().currentUser.selectedAddress;
        if (!ean && (!address || !address.gEan || !address.gEan.ean)) {
            dispatch({
                type: GAS_DASHBOARD_DATA_FAILURE
            });
            return;
        }
        ean = ean || (address || address.gEan || address.gEan.ean);

        const newFilter = {
            ean: address.gEan.ean || ean,
            from:  from || moment().add(isDemo ? -2 : -1, 'day').format('YYYYMMDD'),
            to: from || moment().add(isDemo ? -2 : -1, 'day').format('YYYYMMDD'),
            includePrev: true};

        APIService.getP4Usage(newFilter).then(data => {
            dispatch({
                type: GAS_DASHBOARD_DATA_SUCCESS,
                data
            });
        }, error => {
            dispatch({
                type: GAS_DASHBOARD_DATA_FAILURE,
                error
            });
        });
    };
}

export function getRTLast(items, dateStart, dateEnd) {
    return (dispatch, getStore) => {
        dispatch({
            type: GET_DASHBOARD_RT_DAY_DATA_REQUEST
        });
        const dateStart = moment().add(-2, 'day').format('YYYYMMDD');
        const dateEnd = moment().add(-1, 'day').format('YYYYMMDD');
        let promises = [];
        let itemIds = [];
        for (let itemId in items) {
            itemIds.push(itemId);
            promises.push(APIService.getZabbixHistoryItemData(itemId, dateStart, dateEnd))
        }

        Promise.all(promises).then(data => {
            dispatch({
                type: GET_DASHBOARD_RT_DAY_DATA_SUCCESS,
                data,
                itemIds
            });
        }, error => {
            dispatch({
                type: GET_DASHBOARD_RT_DAY_DATA_FAILURE,
                error
            });
        });
    };
}
export function clearState() {
    return dispatch => {
        dispatch({
            type: CLEAR_CUSTOMERS_STATE
        });
    };
}
