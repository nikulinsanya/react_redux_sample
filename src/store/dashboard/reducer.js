import {
    EL_DASHBOARD_DATA_SUCCESS,
    EL_DASHBOARD_DATA_FAILURE,
    GAS_DASHBOARD_DATA_SUCCESS,
    GAS_DASHBOARD_DATA_FAILURE,
    GET_DASHBOARD_RT_DAY_DATA_SUCCESS,
    GET_DASHBOARD_RT_DAY_DATA_FAILURE,
    CLEAR_CUSTOMERS_STATE
} from './constants';

const ACTION_HANDLERS = {
    [EL_DASHBOARD_DATA_SUCCESS]: (state, action) => {
        const {data} = action;
        return {
            ...state,
            elConsumption: data
        };
    },
    [EL_DASHBOARD_DATA_FAILURE]: (state) => {
        return {
            ...state,
            elConsumption: {}
        };
    },
    [GAS_DASHBOARD_DATA_SUCCESS]: (state, action) => {
        const {data} = action;
        return {
            ...state,
            dashboardGasConsumption: data
        };
    },
    [GET_DASHBOARD_RT_DAY_DATA_FAILURE]: (state) => {
        return {
            ...state,
            rtConsumption: null
        };
    },
    [GET_DASHBOARD_RT_DAY_DATA_SUCCESS]: (state, action) => {
        const { data, itemIds } = action;

        let rtConsumption = {};
        for (let i in itemIds) {
            rtConsumption[itemIds[i]] = data[i];
        }

        return {
            ...state,
            rtConsumption
        };
    },
    [GAS_DASHBOARD_DATA_FAILURE]: (state) => {
        return {
            ...state,
            dashboardGasConsumption: {}
        };
    }

};

const getInitialState = () => {
    return {
        elConsumption: null,
        dashboardGasConsumption: null,
        rtConsumption: null
    };
};

const initialState = getInitialState();

export default function customersReducer(state = initialState, action) {
    const {type} = action;
    const handler = ACTION_HANDLERS[type];
    return handler ? handler(state, action) : state;
};
