import {
    OPEN_SIDEBAR,
    CLOSE_SIDEBAR
} from './constants';

const ACTION_HANDLERS = {
    [OPEN_SIDEBAR]: (state) => {
        return {
            ...state,
            isSidebarOpen: true
        };
    },
    [CLOSE_SIDEBAR]: (state) => {
        return {
            ...state,
            isSidebarOpen: false
        };
    }
};

const initialState = {
    isSidebarOpen: false
};

export default function uiReducer(state = initialState, action) {
    const {type} = action;
    const handler = ACTION_HANDLERS[type];
    return handler ? handler(state, action) : state;
};
