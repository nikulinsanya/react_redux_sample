import { combineReducers } from 'redux';
import authReducer from './auth/reducer';
import dashboardReducer from './dashboard/reducer';
import electricityReducer from './electricity/reducer';
import currentUserReducer from './currentUser/reducer';
import notificationsReducer from './notifications/reducer';
import { routerReducer } from 'react-router-redux';
import uiReducer from './ui/reducer';
import * as storage from 'redux-storage';

export const makeRootReducer = (asyncReducers) => {
    return storage.reducer(combineReducers({
        routing: routerReducer,
        dashboard: dashboardReducer,
        electricity: electricityReducer,
        auth: authReducer,
        currentUser: currentUserReducer,
        notifications: notificationsReducer,
        ui: uiReducer,
        ...asyncReducers
    }));
};

export const injectReducer = (store, {key, reducer}) => {
    if (Object.hasOwnProperty.call(store.asyncReducers, key)) return;

    store.asyncReducers[key] = reducer;
    store.replaceReducer(makeRootReducer(store.asyncReducers));
};

export default makeRootReducer;
