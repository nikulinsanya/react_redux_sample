import {
    GET_ELECTRICITY_DATA_REQUEST,
    GET_ELECTRICITY_DATA_FAILURE,
    GET_ELECTRICITY_DATA_SUCCESS
} from 'store/electricity/constants';

const ACTION_HANDLERS = {
    [GET_ELECTRICITY_DATA_REQUEST]: (state) => {
        return {
            ...state,
            isFetching: true,
            isFailedRequest: false
        };
    },
    [GET_ELECTRICITY_DATA_FAILURE]: (state) => {
        return {
            ...state,
            isFetching: false,
            isFailedRequest: true
        };
    },
    [GET_ELECTRICITY_DATA_SUCCESS]: (state) => {
        return {
            ...state,
            isFetching: false,
            isFailedRequest: false
        };
    }
};

const initialState = {
    isFetching: true,
    isFailedRequest: false
};

export default function uiReducer(state = initialState, action) {
    const {type} = action;
    const handler = ACTION_HANDLERS[type];
    return handler ? handler(state, action) : state;
};
