import React from 'react';
import PropTypes from 'prop-types';
import WidgetPreloader from 'components/WidgetPreloader';
import {CHART_COLORS} from 'constants'
var ReactHighcharts = require('react-highcharts');
var ReactHighstock = require('react-highcharts/ReactHighstock.src');
import _ from 'lodash';
import moment from 'moment';
const co2Index = 414.8527;
class CustomChart extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            loaded: false,
            series: [],
            config: null
        };
    }

    shouldComponentUpdate(nextProps, nextState) {
        return (nextProps.data && !_.isEqual(nextProps.data, this.props.data)) ||
            !_.isEqual(nextState, this.state) ||
            (
                nextProps.showSun !== this.props.showSun ||
                nextProps.showStandBy !== this.props.showStandBy ||
                nextProps.showTemperature !== this.props.showTemperature ||
                nextProps.showCO2 !== this.props.showCO2 ||
                nextProps.showCosts !== this.props.showCosts
            )
    }

    componentWillReceiveProps(nextProps, nextState) {
        if (nextProps.data && !_.isEqual(nextProps.data, this.props.data) ||
            (
                nextProps.showSun !== this.props.showSun ||
                nextProps.showStandBy !== this.props.showStandBy ||
                nextProps.showTemperature !== this.props.showTemperature ||
                nextProps.showCO2 !== this.props.showCO2 ||
                nextProps.showCosts !== this.props.showCosts
            )
        ) {
            this.prepareData(nextProps);
        }
    }

    prepareData(props) {
        const propsData = props.data;

        var series, currentTs, momentTs, i,
            consumption = {peak:[], offpeak:[], total:[]},
            production = {peak:[], offpeak:[], total:[]},
            temperature = [],
            totalInDate,
            costs = {peak:[], offpeak:[], total:[]},
            maxCost = 0,
            costKey = 'cost',
            data = propsData && propsData.aData,
            offset = 0, // moment().utcOffset(),
            key = 'usage';

        let yTitle = data && data.length === 720 ? 'Watt' : 'kWh';
        let totalPerRange = 0;
        let minValue = 0;
        let xAxis = {
            type: 'datetime',
            crosshair: true,
            dateTimeLabelFormats:{
                millisecond: '%Y',
                second: '%H:%M:%S',
                minute: '%H:%M',
                hour: '%H:%M',
                day: '%e. %b',
                week: '%e. %b',
                month: '%b \'%y',
                year: '%Y'
            }
        };
        let yAxises = [];

        i = data && data.length;

        while (i--) {
            momentTs = moment(data[i].dateFrom).add(-offset, 'minutes');
            currentTs = Date.UTC(
                momentTs.year(), momentTs.month(), momentTs.date(), momentTs.hours(), momentTs.minutes()
            );
            if (!data[i][key]) {
                data[i][key] = [0, 0];
            }

            data[i][key]['r181'] = data[i][key]['r181'] && Math.abs(data[i][key]['r181']) || 0;
            consumption.peak.push([currentTs, (data[i][key]['r181'] || null)]);

            data[i][key]['r182'] = data[i][key]['r182'] && Math.abs(data[i][key]['r182']) || 0;
            consumption.offpeak.push([currentTs, (data[i][key]['r182'] || null)]);

            consumption.total.push([currentTs, data[i][key]['r181'] + data[i][key]['r182'] || 0]);

            data[i][key]['r281'] = data[i][key]['r281'] ? Math.abs(data[i][key]['r281']) : data[i][key]['r281'];
            production.peak.push([currentTs, data[i][key]['r281'] || null]);

            data[i][key]['r282'] = data[i][key]['r282'] ? Math.abs(data[i][key]['r282']) : data[i][key]['r282'];
            production.offpeak.push([currentTs, data[i][key]['r282'] || null]);

            production.total.push([currentTs, data[i][key]['r281'] + data[i][key]['r282'] || 0]);

            totalInDate = data[i][key]['r181'] + data[i][key]['r182'] || 0;
            totalPerRange += totalInDate;
            minValue = minValue || totalInDate;
            minValue = totalInDate && totalInDate > minValue ? minValue : totalInDate;
            if (data[i].temperature) {
                temperature.push([currentTs, data[i].temperature]);
            }
        }
        consumption.peak.sort(function(a, b) {
            return a[0] - b[0];
        });
        consumption.total.sort(function(a, b) {
            return a[0] - b[0];
        });
        consumption.offpeak.sort(function(a, b) {
            return a[0] - b[0];
        });
        production.peak.sort(function(a, b) {
            return a[0] - b[0];
        });
        production.total.sort(function(a, b) {
            return a[0] - b[0];
        });
        production.offpeak.sort(function(a, b) {
            return a[0] - b[0];
        });
        temperature.sort(function (a, b) {
            return a[0] - b[0];
        });

        series = [];
        if (consumption.peak.some(function(elem) { return elem[1] != null })) {
            series.push({
                name: 'Consumptie laag',
                data: consumption.peak,
                stack: 'consumption',
                index: 1,
                legendIndex: 0,
                color: CHART_COLORS[0],
                tooltip:{
                    pointFormat: '<span style="color:{point.color}">\u25CF</span> {series.name}: <b>{point.y:.3f} ' +
                    yTitle + '</b><br/>'
                }
            });
        }

        if (consumption.offpeak.some(function(elem) { return elem[1] != null })) {
            series.push({
                name: 'Consumptie hoog',
                data: consumption.offpeak,
                stack: 'consumption',
                index: 1,
                legendIndex: 1,
                color: CHART_COLORS[1],
                tooltip:{
                    pointFormat: '<span style="color:{point.color}">\u25CF</span> {series.name}: <b>{point.y:.3f} ' +
                    yTitle + '</b><br/>'
                }
            });
        }

        if (production.peak.some(function(elem) { return elem[1] != null })) {
            series.push({
                name: 'Terug levering laag tarief',
                data:  production.peak,
                stack: 'consumption',
                index: 0,
                legendIndex: 2,
                color: CHART_COLORS[10],
                tooltip:{
                    pointFormat: '<span style="color:{point.color}">\u25CF</span> {series.name}: <b>{point.y:.3f} ' +
                    yTitle + '</b><br/>'
                }
            });
        }

        if (production.offpeak.some(function(elem) { return elem[1] != null })) {
            series.push({
                name: 'Terug levering hoog tarief',
                data:  production.offpeak,
                stack: 'consumption',
                index: 0,
                legendIndex: 3,
                color: CHART_COLORS[11],
                tooltip:{
                    pointFormat: '<span style="color:{point.color}">\u25CF</span> {series.name}: <b>{point.y:.3f} ' +
                    yTitle + '</b><br/>'
                }
            });
        }

        for (var i = 0; i <= consumption.peak.length; i++) {
            if ((consumption.peak[i] && !consumption.peak[i].y && consumption.peak[i + 1] && !!consumption.peak[i + 1].y) &&
                (consumption.offpeak[i - 1] && !!consumption.offpeak[i - 1].y)) {
                consumption.peak[i].y = consumption.offpeak[i - 1].y;
            }
        }

        for (var i = 0; i <= consumption.offpeak.length; i++) {
            if ((consumption.offpeak[i] && !consumption.offpeak[i].y && consumption.offpeak[i + 1] && !!consumption.offpeak[i + 1].y) &&
                (consumption.peak[i - 1] && !!consumption.peak[i - 1].y)) {
                consumption.offpeak[i].y = consumption.peak[i - 1].y;
            }
        }

        var mainAxis = {
            labels: {
                format: '{value} ' + yTitle
            },
            title: {
                text: yTitle
            },
            min:0
        };

        if (props.showStandBy) {
            mainAxis.plotLines = [{
                color: 'red',
                value: minValue,
                width: '2',
                zIndex: 5,
                showInNavigator: false,
                label: {
                    text: (minValue * data.length).toLocaleString('nl-NL', { maximumFractionDigits: 1 }) + yTitle + ' (' +
                    (minValue * data.length / totalPerRange * 100).toLocaleString('nl-NL', { maximumFractionDigits: 1 }) + '% ',
                    align: 'right',
                    x: 5,
                    y: 16,
                    style: {
                        color: 'red',
                        fontWeight: 'bold',
                        backgroundColor:'white'
                    }
                }
            }]
        }

        if (propsData && props.showSun && propsData.sunInfo) {
            var startDay = moment(propsData.sunInfo.sunRise).startOf('day'),
                endDay = moment(propsData.sunInfo.sunRise).add(1, 'day').startOf('day').add(moment()
                    .utcOffset(), 'minutes');

            xAxis.plotBands = [
                {
                    from: startDay,
                    to:  moment(propsData.sunInfo.sunRise).add(moment().utcOffset(), 'minutes'),
                    color: 'rgba(68, 170, 213, .1)'
                },
                {
                    from:  moment(propsData.sunInfo.sunRise).add(moment().utcOffset(), 'minutes'),
                    to:   moment(propsData.sunInfo.sunSet).add(moment().utcOffset(), 'minutes'),
                    color: 'rgba(255, 236, 10, 0.1)',
                    label: {text:'Daglicht'}
                },
                {
                    from:  moment(propsData.sunInfo.sunSet).add(moment().utcOffset(), 'minutes'),
                    to:  endDay,
                    color: 'rgba(68, 170, 213, .1)'
                }]
        }

        yAxises.push(mainAxis);

        if (props.showCosts) {
            i = data.length;
            while (i--) {
                momentTs = moment(data[i].dateFrom).add(-offset, 'minutes');
                currentTs = Date.UTC(
                    momentTs.year(), momentTs.month(), momentTs.date(), momentTs.hours(), momentTs.minutes()
                );
                if (data[i][costKey]) {
                    costs.peak.push([currentTs, (data[i][costKey]['r181'] || 0)]);
                    costs.offpeak.push([currentTs, (data[i][costKey]['r182'] || 0)]);
                    costs.total.push([currentTs, data[i][costKey]['r181'] +
                    data[i][costKey]['r182'] +
                    data[i][costKey]['r281'] +
                    data[i][costKey]['r282'] ]);
                    maxCost = (data[i][costKey]['r181'] + data[i][costKey]['r182'] || 0) > maxCost
                        ? data[i][costKey]['r181'] + data[i][costKey]['r182'] || 0
                        : maxCost;
                }
            }

            costs.total.sort(function(a, b) {
                return a[0] - b[0];
            });
            series.push({
                yAxis: yAxises.length,
                name: 'Kosten',
                type: 'spline',
                data: costs.total,
                pointPlacement: 'on',
                pointPadding: 0.2,
                color: '#149A45',
                showInNavigator: false,
                tooltip: {
                    pointFormat:
                        '<span style="color:{point.color}">\u25CF</span> {series.name}: <b>{point.y:.2f} EUROS</b><br/>'
                }
            });
            yAxises.push({
                gridLineWidth: 0,
                title: {
                    text: 'SPENDS'
                },
                labels: {
                    format: '{value} EUROS',
                    style:{
                        color: '#117733'
                    }
                },
                opposite: true
            });
        }

        if (props.showTemperature) {
            series.push({
                yAxis: yAxises.length,
                name: 'Temperatuur',
                type: 'spline',
                data: temperature,
                tooltip: {
                    valueSuffix: ' °C'
                },
                color: '#FF0000',
                showInNavigator: false,
                negativeColor: '#0031AB'
            });
            yAxises.push({
                gridLineWidth: 0,
                title: {
                    text: 'Temperatuur'
                },
                labels: {
                    format: '{value} °C',
                    style:{
                        color: '#117733'
                    }
                }
            });
        }

        if (props.showCO2) {
            let co2Values = {};
            let co2Series = []


            if (consumption.peak.some((elem) => elem[1] != null)) {
                consumption.peak.forEach((item) => {
                    co2Values[item[0]] = co2Values[item[0]] || item[1];
                });
            }
            if (consumption.offpeak.some((elem) => elem[1] != null)) {
                consumption.offpeak.forEach((item) => {
                    co2Values[item[0]] = co2Values[item[0]] || item[1];
                });
            }
            if (production.offpeak.some((elem) => elem[1] != null)) {
                production.offpeak.forEach((item) => {
                    co2Values[item[0]] = co2Values[item[0]] ||  (-1 * item[1]);
                });
            }
            if (production.peak.some((elem) => elem[1] != null)) {
                production.peak.forEach((item) => {
                    co2Values[item[0]] = co2Values[item[0]] ||  (-1 * item[1]);
                });
            }


            for(let i in co2Values){
                co2Series.push([parseInt(i), parseInt(co2Values[i] * co2Index)])
            }

            series.push({
                yAxis: yAxises.length,
                name: 'CO2-uitstoot',
                data:  co2Series,
                type: 'spline',
                color: '#a570a2',
                tooltip:{
                    pointFormat: '<span style="color:{point.color}">\u25CF</span> {series.name}: <b>{point.y} gramm</b><br/>'
                }
            });
            yAxises.push({
                gridLineWidth: 0,
                title: {
                    text: 'CO2-uitstoot'
                },
                labels: {
                    format: '{value} gr',
                    style:{
                        color: '#a570a2'
                    }
                }
            });
        }

        let config = {
            legend: {
                shadow: false
            },
            credits: {
                enabled: false
            },
            chart: {
                type:  data && data.length === 720 ? 'area' : 'column',
                zoomType: 'x',
                animation: ReactHighcharts.svg, // don't animate in old IE
                marginRight: 10,
                height: 580
            },

            title: {
                text: ''
            },

            xAxis: xAxis,
            yAxis: yAxises,

            tooltip: {
                shared: true
            },
            exporting: {
                enabled: false
            },
            plotOptions: {
                series: {
                    compare: 'percent',
                    showInNavigator: true
                }
                // area: {
                //     marker: {
                //         enabled: false,
                //         symbol: 'circle',
                //         radius: 2,
                //         states: {
                //             hover: {
                //                 enabled: true
                //             }
                //         }
                //     }
                // },

            },
            rangeSelector: {
                buttons: [

                    {
                        type: 'all',
                        text: 'All'
                    },
                    {
                        type: 'minute',
                        count: 10,
                        text: '10m'
                    },
                    {
                        type: 'minute',
                        count: 30,
                        text: '30m'
                    },
                    {
                        type: 'hour',
                        count: 1,
                        text: '1uur'
                    }],
                selected: 0,
                allButtonsEnabled: true,
                inputEnabled: false
            },
            series: series
        }

        this.setState({config: config, loaded: true});

        return config;
    }

    componentDidMount() {
        this.prepareData(this.props);
    }

    renderChartType() {
        const config = this.state.config;
        const data = this.props.data;
        return data && data.aData && data.aData.length === 720
            ? <ReactHighstock config={config}
                             ref="chart"
                             callback={chart => {
                                 this.chart = chart;
                             }}/>
            : <ReactHighcharts config={config}
                         ref="chart"
                         callback={chart => {
                             this.chart = chart;
                         }}/>
    }

    render() {
        return (
            this.state.loaded
            ? this.renderChartType()
            : <WidgetPreloader/>

        );
    }
}

CustomChart.propTypes = {
    data: PropTypes.object,
    showSun: PropTypes.bool,
    showStandBy: PropTypes.bool,
    showTemperature: PropTypes.bool,
    showCO2: PropTypes.bool,
    showCosts: PropTypes.bool
};

export default CustomChart;
