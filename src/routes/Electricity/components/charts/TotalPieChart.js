import React from 'react';
import PropTypes from 'prop-types';
import WidgetPreloader from 'components/WidgetPreloader';
import {CHART_COLORS} from 'constants'
var ReactHighcharts = require('react-highcharts');
import _ from 'lodash';
import moment from 'moment';

class TotalPieChart extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            loaded: false,
            series: [],
            config: null
        };
    }

    shouldComponentUpdate(nextProps, nextState) {
        return (nextProps.data && !_.isEqual(nextProps.data, this.props.data)) ||
            !_.isEqual(nextState, this.state)
    }

    componentWillReceiveProps(nextProps, nextState) {
        if (nextProps.data && !_.isEqual(nextProps.data, this.props.data)
        ) {
            this.prepareData(nextProps);
        }
    }

    prepareData(props) {
        const propsData = props.data;

        let data = propsData && propsData.aData;
        let prodPeak = 0,
            prodOffPeak = 0,
            consOffPeak = 0,
            consPeak = 0,
            i = data && data.length,
            series,
            key = 'usage',
            title = 'kwh';

        while (i--) {
            consPeak += data[i][key] && data[i][key]['r181'] || 0;
            consOffPeak += data[i][key] && data[i][key]['r182'] || 0;

            prodPeak += data[i][key] && data[i][key]['r281'] || 0;
            prodOffPeak += data[i][key] && data[i][key]['r282'] || 0;
        }
        series = [
            {
                name: 'Consumptie laag',
                y: Math.abs(consPeak.toFixed(2)),
                color: CHART_COLORS[0],
                tooltip: {
                    valueSuffix: ' ' + title
                }
            },
            {
                name: 'Consumptie hoog',
                y: Math.abs(consOffPeak.toFixed(2)),
                color: CHART_COLORS[1],
                tooltip: {
                    valueSuffix: ' ' + title
                }
            }
        ];
        if (prodPeak) {
            series.push(
                {
                    name: 'Terug levering laag tarief',
                    y: Math.abs(prodPeak.toFixed(2)),
                    color:  CHART_COLORS[10],
                    tooltip: {
                        valueSuffix: ' ' + title
                    }
                }
            )
        }
        if (prodOffPeak) {
            series.push(
                {
                    name: 'Terug levering hoog tarief',
                    y: Math.abs(prodOffPeak.toFixed(2)),
                    color:CHART_COLORS[11],
                    tooltip: {
                        valueSuffix: ' ' + title
                    }
                }
            )
        }

        let config = {
            credits: {enabled: false},
            chart: {
                type: 'pie',
                options3d: {
                    enabled: true,
                    alpha: 45,
                    beta: 0
                }
            },
            title: {
                text: ''
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    depth: 35,
                    showInLegend: true
                }
            },

            series: [{
                type: 'pie',
                name: title,
                data: series
            }]
        }

        this.setState({config: config, loaded: true});

        return config;
    }

    componentDidMount() {
        this.prepareData(this.props);
    }

    render() {
        const config = this.state.config;
        return (
            this.state.loaded
            ? <ReactHighcharts config={config}
                               ref="chart"
                               callback={chart => {
                                   this.chart = chart;
                               }}/>
            : <WidgetPreloader/>

        );
    }
}

TotalPieChart.propTypes = {
    data: PropTypes.object
};

export default TotalPieChart;
