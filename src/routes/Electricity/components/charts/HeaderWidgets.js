import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import WidgetPreloader from 'components/WidgetPreloader';
import MaxKwh from './MaxKwh';
import moment from 'moment';

class HeaderWidgets extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            hasElProd: false
        };
    }
    componentWillReceiveProps(nextProps, nextState) {
        if (nextProps.data && !_.isEqual(nextProps.data, this.props.data)
        ) {
            const { data } = nextProps;

            if (!data) {
                return;
            }
            const { totalUsage } = data;

            const hasElProd = totalUsage && (totalUsage.r281 || totalUsage.r282);
            this.setState({
                hasElProd
            });
        }
    }

    renderTotalUsage() {
        const { data } = this.props;

        const { totalUsage, totalCost, totalUsagePrev } = data;
        const total = totalUsage && (totalUsage.r181 + totalUsage.r182) || 0;
        const prev = totalUsagePrev && (totalUsagePrev.r181 + totalUsagePrev.r182) || 0;
        const trend = total && prev ? (100 - (100 * prev / total)).toLocaleString('nl-NL', { maximumFractionDigits: 1 }) : 0;
        const { hasElProd } = this.state;
        const widgetClass = cx({
            'col-lg-4 col-md-4 col-sm-6':  true
        });
        return (
            data && data.totalUsage && (data.totalUsage.r181 || data.totalUsage.r181 === 0)
            ? <div className={widgetClass}>
                <div className="panel panel-primary">
                    <div className="panel-heading">
                        Totale verbruik
                    </div>
                    <div className="panel-heading">
                        <div className="row">
                            <div className="col-xs-3">
                                <i className="fa fa-level-down fa-5x"/>
                            </div>
                            <div className="col-xs-9 text-right">
                                <div className="huge">
                                    {(data.totalUsage &&
                                        (data.totalUsage.r181 + (data.totalUsage.r182 || 0)).toLocaleString('nl-NL', { maximumFractionDigits: 2 }) || 0)}
                                        kWh
                                </div>
                                <div>
                                    {trend} %
                                    <i className="fa fa-caret-down" aria-hidden="true"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="panel-footer">
                        <span className="pull-right">
                            Laag: {(data.totalUsage && data.totalUsage.r181 || 0).toLocaleString('nl-NL', { maximumFractionDigits: 2 })} kWh <br/>
                            Hoog: {(data.totalUsage && data.totalUsage.r182 || 0).toLocaleString('nl-NL', { maximumFractionDigits: 2 })} kWh
                        </span>
                        <div className="clearfix" />
                    </div>
                </div>
            </div>

                : <div className={widgetClass}>
                    <div className="panel panel-primary">
                        <div className="panel-heading">
                            Totale verbruik
                        </div>
                        <div className="panel-heading">
                            <div className="row">
                                <div className="col-xs-3">
                                    <i className="fa fa-level-down fa-5x"/>
                                </div>
                                <div className="col-xs-9 text-right">
                                    <div>
                                        Geen data
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="panel-footer">
                            <span className="pull-right" />
                            <div className="clearfix" />
                        </div>
                    </div>
                </div>

        );
    }
    renderTotalProduction() {
        const {data} = this.props;
        const { totalUsage, totalCost, totalUsagePrev } = data;
        const total = totalUsage && (totalUsage.r281 + totalUsage.r282) || 0;
        const prev = totalUsagePrev && (totalUsagePrev.r281 + totalUsagePrev.r282) || 0;
        const trend = total && prev ? (100 - (100 * prev / total)).toLocaleString('nl-NL', { maximumFractionDigits: 1 }) : 0;
        const trendIcoClass = cx({'fa fa-caret-up': parseFloat(trend) > 0, 'fa fa-caret-down': parseFloat(trend) < 0});

        if (!this.state.hasElProd) {
            return null;
        }
        return (
            data && data.totalUsage && (data.totalUsage.r281 || data.totalUsage.r281 === 0)
                ? <div className="col-lg-4 col-md-4 col-sm-6">
                    <div className="panel panel-primary">
                        <div className="panel-heading">
                            Totale teruglevering
                        </div>
                        <div className="panel-heading">
                            <div className="row">
                                <div className="col-xs-3">
                                    <i className="fa fa-level-up fa-5x"/>
                                </div>
                                <div className="col-xs-9 text-right">
                                    <div className="huge">
                                        {data.totalUsage
                                            ? ((data.totalUsage.r281 + data.totalUsage.r282).toLocaleString('nl-NL', { maximumFractionDigits: 2 })) : 0} kWh
                                    </div>
                                    <div>
                                        {trend} %
                                        <i className={trendIcoClass} aria-hidden="true"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="panel-footer">
                            <span className="pull-right">
                                Laag: {data.totalUsage && (data.totalUsage.r281 || 0).toLocaleString('nl-NL', { maximumFractionDigits: 2 })} kWh <br/>
                                Hoog: {data.totalUsage && (data.totalUsage.r282 || 0).toLocaleString('nl-NL', { maximumFractionDigits: 2 })} kWh            </span>
                            <div className="clearfix" />
                        </div>
                    </div>
                </div>
                : <div className="col-lg-4 col-md-4 col-sm-6">
                    <div className="panel panel-primary">
                        <div className="panel-heading">
                            Totale teruglevering
                        </div>
                        <div className="panel-heading">
                            <div className="row">
                                <div className="col-xs-3">
                                    <i className="fa fa-level-down fa-5x"/>
                                </div>
                                <div className="col-xs-9 text-right">
                                    <div>
                                        Geen data
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="panel-footer">
                            <span className="pull-right" />
                            <div className="clearfix" />
                        </div>
                    </div>
                </div>
        )
    }

    renderTotalCost() {
        const {data} = this.props;
        const { totalCost, totalCostPrev } = data;
        const total = totalCost && (totalCost.r181 + totalCost.r182 + totalCost.r281 + totalCost.r282) || 0;
        const prev = totalCostPrev &&
            (totalCostPrev.r181 + totalCostPrev.r182 + totalCostPrev.r281 + totalCostPrev.r282) || 0;
        const trend = total && prev ? (100 - (100 * prev / total)).toLocaleString('nl-NL', { maximumFractionDigits: 1 }) : 0;
        const trendIcoClass = cx({'fa fa-caret-up': parseFloat(trend) > 0, 'fa fa-caret-down': parseFloat(trend) < 0});
        const { hasElProd } = this.state;
        const widgetClass = cx({
            'col-lg-4 col-md-4 col-sm-6':  true
        });
        return (
            data && data.totalCost && (data.totalCost.r181 || data.totalCost.r181 === 0)
                ? <div className={widgetClass}>
                    <div className="panel panel-primary">
                        <div className="panel-heading">
                            Totale kosten
                        </div>
                        <div className="panel-heading">
                            <div className="row">
                                <div className="col-xs-3">
                                    <i className="fa fa-money fa-5x"/>
                                </div>
                                <div className="col-xs-9 text-right">
                                    <div className="huge">€ {
                                        (data.totalCost.r181 +
                                            data.totalCost.r182 +
                                            data.totalCost.r281 +
                                            data.totalCost.r282).toLocaleString('nl-NL', { maximumFractionDigits: 2 })}
                                    </div>
                                    <div>
                                        {trend} %
                                        <i className={trendIcoClass} aria-hidden="true"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="panel-footer">
                            <span className="pull-right">
                                Laag: € {(data.totalCost.r181 + data.totalCost.r281).toLocaleString('nl-NL', { maximumFractionDigits: 2 })} <br/>
                                Hoog: € {(data.totalCost.r182 + data.totalCost.r282).toLocaleString('nl-NL', { maximumFractionDigits: 2 })} </span>
                            <div className="clearfix" />
                        </div>
                    </div>
                </div>
                : <div className={widgetClass}>
                    <div className="panel panel-primary">
                        <div className="panel-heading">
                            Totale kosten
                        </div>
                        <div className="panel-heading">
                            <div className="row">
                                <div className="col-xs-3">
                                    <i className="fa fa-money fa-5x"/>
                                </div>
                                <div className="col-xs-9 text-right">
                                    <div>
                                        Geen data
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="panel-footer">
                            <span className="pull-right" />
                            <div className="clearfix" />
                        </div>
                    </div>
                </div>
        )
    }

    render() {
        const { data, maxKwh} = this.props;
        return (
            data && !this.props.preloader
            ? <div className="row">
                    {this.renderTotalUsage()}
                    {this.renderTotalProduction()}
                    {this.renderTotalCost()}
                    {maxKwh ?
                        <div className="col-lg-4 col-md-4 col-sm-6"><MaxKwh maxKwh={maxKwh}/></div>
                        : null}
                </div>
            : <WidgetPreloader/>

        );
    }
}

HeaderWidgets.propTypes = {
    data: PropTypes.object,
    preloader: PropTypes.bool,
    maxKwh: PropTypes.object
};

export default HeaderWidgets;
