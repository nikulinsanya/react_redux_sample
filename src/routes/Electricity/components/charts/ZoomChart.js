import React from 'react';
import PropTypes from 'prop-types';
import WidgetPreloader from 'components/WidgetPreloader';
import {CHART_COLORS} from 'constants'
var ReactHighcharts = require('react-highcharts');
var ReactHighstock = require('react-highcharts/ReactHighstock.src');
import _ from 'lodash';
import moment from 'moment';

class ZoomChart extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            loaded: false,
            series: [],
            config: null
        };
    }

    shouldComponentUpdate(nextProps, nextState) {
        return (nextProps.data && !_.isEqual(nextProps.data, this.props.data)) ||
            !_.isEqual(nextState, this.state) ||
            (
                nextProps.showSun !== this.props.showSun ||
                nextProps.showStandBy !== this.props.showStandBy ||
                nextProps.showTemperature !== this.props.showTemperature ||
                nextProps.showCosts !== this.props.showCosts
            )
    }

    componentWillReceiveProps(nextProps, nextState) {
        if (nextProps.data && !_.isEqual(nextProps.data, this.props.data) ||
            (
                nextProps.showSun !== this.props.showSun ||
                nextProps.showStandBy !== this.props.showStandBy ||
                nextProps.showTemperature !== this.props.showTemperature ||
                nextProps.showCosts !== this.props.showCosts
            )
        ) {
            this.prepareData(nextProps);
        }
    }

    prepareData(props) {
        const propsData = props.data;

        // if (!propsData || !propsData.aData) {
        //     this.setState({loaded: true});
        //     return;
        // }

        var series, currentTs, momentTs, i,
            consumption = {peak:[], offpeak:[], total:[]},
            production = {peak:[], offpeak:[], total:[]},
            temperature = [],
            totalInDate,
            costs = {peak:[], offpeak:[], total:[]},
            maxCost = 0,
            costKey = 'cost',
            data = propsData && propsData.aData,
            offset = 0, // moment().utcOffset(),
            key = 'usage';

        let yTitle = 'kWh';
        let totalPerRange = 0;
        let minValue = 0;
        let xAxis = {
            type: 'datetime',
            crosshair: true,
            dateTimeLabelFormats:{
                millisecond: '%Y',
                second: '%H:%M:%S',
                minute: '%H:%M',
                hour: '%H:%M',
                day: '%e. %b',
                week: '%e. %b',
                month: '%b \'%y',
                year: '%Y'
            }
        };
        let yAxises = [];

        i = data && data.length;

        while (i--) {
            momentTs = moment(data[i].dateFrom).add(-offset, 'minutes');
            currentTs = Date.UTC(
                momentTs.year(), momentTs.month(), momentTs.date(), momentTs.hours(), momentTs.minutes()
            );
            if (!data[i][key]) {
                data[i][key] = [0, 0];
            }

            data[i][key]['r181'] = data[i][key]['r181'] && Math.abs(data[i][key]['r181']) || 0;
            consumption.peak.push([currentTs, (data[i][key]['r181'] || null)]);

            data[i][key]['r182'] = data[i][key]['r182'] && Math.abs(data[i][key]['r182']) || 0;
            consumption.offpeak.push([currentTs, (data[i][key]['r182'] || null)]);

            consumption.total.push([currentTs, data[i][key]['r181'] + data[i][key]['r182'] || 0]);

            data[i][key]['r281'] = data[i][key]['r281'] ? Math.abs(data[i][key]['r281']) : data[i][key]['r281'];
            production.peak.push([currentTs, data[i][key]['r281'] || null]);

            data[i][key]['r282'] = data[i][key]['r282'] ? Math.abs(data[i][key]['r282']) : data[i][key]['r282'];
            production.offpeak.push([currentTs, data[i][key]['r282'] || null]);

            production.total.push([currentTs, data[i][key]['r281'] + data[i][key]['r282'] || 0]);

            totalInDate = data[i][key]['r181'] + data[i][key]['r182'] || 0;
            totalPerRange += totalInDate;
            minValue = minValue || totalInDate;
            minValue = totalInDate && totalInDate > minValue ? minValue : totalInDate;
            if (data[i].temperature) {
                temperature.push([currentTs, data[i].temperature]);
            }
        }
        consumption.peak.sort(function(a, b) {
            return a[0] - b[0];
        });
        consumption.total.sort(function(a, b) {
            return a[0] - b[0];
        });
        consumption.offpeak.sort(function(a, b) {
            return a[0] - b[0];
        });
        production.peak.sort(function(a, b) {
            return a[0] - b[0];
        });
        production.total.sort(function(a, b) {
            return a[0] - b[0];
        });
        production.offpeak.sort(function(a, b) {
            return a[0] - b[0];
        });
        temperature.sort(function (a, b) {
            return a[0] - b[0];
        });

        series = [];
        if (consumption.peak.some(function(elem) { return elem[1] != null })) {
            series.push({
                name: 'Consumptie laag',
                data: consumption.peak,
                stack: 'consumption',
                index: 1,
                legendIndex: 0,
                color: CHART_COLORS[0],
                tooltip:{
                    pointFormat: '<span style="color:{point.color}">\u25CF</span> {series.name}: <b>{point.y:.3f} ' +
                    'kWh' + '</b><br/>'
                }
            });
        }

        if (consumption.offpeak.some(function(elem) { return elem[1] != null })) {
            series.push({
                name: 'Consumptie hoog',
                data: consumption.offpeak,
                stack: 'consumption',
                index: 1,
                legendIndex: 1,
                color: CHART_COLORS[1],
                tooltip:{
                    pointFormat: '<span style="color:{point.color}">\u25CF</span> {series.name}: <b>{point.y:.3f} ' +
                    'kWh' + '</b><br/>'
                }
            });
        }

        if (production.peak.some(function(elem) { return elem[1] != null })) {
            series.push({
                name: 'Terug levering laag tarief',
                data:  production.peak,
                stack: 'consumption',
                index: 0,
                legendIndex: 2,
                color: CHART_COLORS[10],
                tooltip:{
                    pointFormat: '<span style="color:{point.color}">\u25CF</span> {series.name}: <b>{point.y:.3f} ' +
                    'kWh' + '</b><br/>'
                }
            });
        }

        if (production.offpeak.some(function(elem) { return elem[1] != null })) {
            series.push({
                name: 'Terug levering hoog tarief',
                data:  production.offpeak,
                stack: 'consumption',
                index: 0,
                legendIndex: 3,
                color: CHART_COLORS[11],
                tooltip:{
                    pointFormat: '<span style="color:{point.color}">\u25CF</span> {series.name}: <b>{point.y:.3f} ' +
                    'kWh' + '</b><br/>'
                }
            });
        }

        var mainAxis = {
            labels: {
                format: '{value} kWh'
            },
            title: {
                text: 'kWh'
            },
            min:0
        };

        if (props.showStandBy) {
            mainAxis.plotLines = [{
                color: 'red',
                value: minValue,
                width: '2',
                zIndex: 5,
                label: {
                    text: (minValue * data.length).toLocaleString('nl-NL', { maximumFractionDigits: 1 }) + 'kWh (' +
                    (minValue * data.length / totalPerRange * 100).toLocaleString('nl-NL', { maximumFractionDigits: 1 }) + '% ',
                    align: 'right',
                    x: 5,
                    y: 16,
                    style: {
                        color: 'red',
                        fontWeight: 'bold',
                        backgroundColor:'white'
                    }
                }
            }]
        }

        if (propsData && props.showSun && propsData.sunInfo) {
            var startDay = moment(propsData.sunInfo.sunRise).startOf('day'),
                endDay = moment(propsData.sunInfo.sunRise).add(1, 'day').startOf('day').add(moment()
                    .utcOffset(), 'minutes');

            xAxis.plotBands = [
                {
                    from: startDay,
                    to:  moment(propsData.sunInfo.sunRise).add(moment().utcOffset(), 'minutes'),
                    color: 'rgba(68, 170, 213, .1)'
                },
                {
                    from:  moment(propsData.sunInfo.sunRise).add(moment().utcOffset(), 'minutes'),
                    to:   moment(propsData.sunInfo.sunSet).add(moment().utcOffset(), 'minutes'),
                    color: 'rgba(255, 236, 10, 0.1)',
                    label: {text:'Daglicht'}
                },
                {
                    from:  moment(propsData.sunInfo.sunSet).add(moment().utcOffset(), 'minutes'),
                    to:  endDay,
                    color: 'rgba(68, 170, 213, .1)'
                }]
        }

        yAxises.push(mainAxis);

        if (props.showCosts) {
            i = data.length;
            while (i--) {
                momentTs = moment(data[i].dateFrom).add(-offset, 'minutes');
                currentTs = Date.UTC(
                    momentTs.year(), momentTs.month(), momentTs.date(), momentTs.hours(), momentTs.minutes()
                );
                if (data[i][costKey]) {
                    costs.peak.push([currentTs, (data[i][costKey]['r181'] || 0)]);
                    costs.offpeak.push([currentTs, (data[i][costKey]['r182'] || 0)]);
                    costs.total.push([currentTs, data[i][costKey]['r181'] +
                    data[i][costKey]['r182'] +
                    data[i][costKey]['r281'] +
                    data[i][costKey]['r282'] ]);
                    maxCost = (data[i][costKey]['r181'] + data[i][costKey]['r182'] || 0) > maxCost
                        ? data[i][costKey]['r181'] + data[i][costKey]['r182'] || 0
                        : maxCost;
                }
            }

            costs.total.sort(function(a, b) {
                return a[0] - b[0];
            });
            series.push({
                yAxis: yAxises.length,
                name: 'Kosten',
                type: 'spline',
                data: costs.total,
                pointPlacement: 'on',
                pointPadding: 0.2,
                color: '#149A45',
                tooltip: {
                    pointFormat:
                        '<span style="color:{point.color}">\u25CF</span> {series.name}: <b>{point.y:.2f} EUROS</b><br/>'
                }
            });
            yAxises.push({
                gridLineWidth: 0,
                title: {
                    text: 'SPENDS'
                },
                labels: {
                    format: '{value} EUROS',
                    style:{
                        color: '#117733'
                    }
                },
                opposite: true
            });
        }

        if (props.showTemperature) {
            series.push({
                yAxis: yAxises.length,
                name: 'Temperatuur',
                type: 'spline',
                data: temperature,
                tooltip: {
                    valueSuffix: ' °C'
                },
                color: '#FF0000',
                negativeColor: '#0031AB'
            });
            yAxises.push({
                gridLineWidth: 0,
                title: {
                    text: 'Temperatuur'
                },
                labels: {
                    format: '{value} °C',
                    style:{
                        color: '#117733'
                    }
                }
            });
        }

        console.log(333)
        let config = {
            legend: {
                shadow: false
            },
            credits: {
                enabled: false
            },
            chart: {
                type: 'column',
                zoomType: 'xy',
                animation: ReactHighcharts.svg, // don't animate in old IE
                marginRight: 10,
                height: 480
            },

            title: {
                text: ''
            },

            xAxis: xAxis,
            yAxis: yAxises,

            tooltip: {
                shared: true
            },
            exporting: {
                enabled: false
            },
            plotOptions: {
                series: {
                    pointPadding: 0,
                    groupPadding: 0,
                    borderWidth: 0.5,
                    shadow: false,
                    marker: {
                        enabled: false
                    },
                    compare: 'percent',
                    showInNavigator: true
                },
                area: {
                    marker: {
                        enabled: false,
                        symbol: 'circle',
                        radius: 2,
                        states: {
                            hover: {
                                enabled: true
                            }
                        }
                    }
                },
                column: {
                    stacking: 'normal'
                }
            },
            rangeSelector: {
                buttons: [{
                    type: 'day',
                    count: 1,
                    text: '1d'
                }, {
                    type: 'week',
                    count: 1,
                    text: '1W'
                }, {
                    type: 'all',
                    count: 1,
                    text: 'All'
                }],
                selected: 1,
                inputEnabled: false
            },
            series: series
        }

        this.setState({config: config, loaded: true});

        return config;
    }

    componentDidMount() {
        this.prepareData(this.props);
    }

    render() {
        const config = this.state.config;
        return (
            this.state.loaded
            ? <ReactHighstock config={config}
                               ref="chart"
                               callback={chart => {
                                   this.chart = chart;
                               }}/>
            : <WidgetPreloader/>

        );
    }
}

ZoomChart.propTypes = {
    data: PropTypes.object,
    showSun: PropTypes.bool,
    showStandBy: PropTypes.bool,
    showTemperature: PropTypes.bool,
    showCosts: PropTypes.bool
};

export default ZoomChart;
