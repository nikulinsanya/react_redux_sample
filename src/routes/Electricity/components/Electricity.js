import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import CustomChart from './charts/CustomChart';
import ZoomChart from './charts/ZoomChart';
import TotalPieChart from './charts/TotalPieChart';
import HeaderWidgets from './charts/HeaderWidgets';
import moment from 'moment';
import DateRangePickerWrapper from 'components/DateRangePickerWrapper';

class Electricity extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            from: moment().add(-2, 'days'),
            to: moment().add(-2, 'days'),
            openChartSettings: false,
            showTemperature: true,
            showCO2: false,
            showCosts: false,
            showStandBy: false,
            showSun: true
        };
        this.onDateRangeChange = this.onDateRangeChange.bind(this);
        this.filter = this.filter.bind(this);
        this.toggleChartSettings = this.toggleChartSettings.bind(this);
    }

    componentDidMount() {
        this.filter();
    }

    componentWillReceiveProps(newVal) {
        if ((newVal.selectedAddress && !this.props.selectedAddress) ||
            (newVal.selectedAddress && this.props.selectedAddress && this.props.selectedAddress.eEan &&
            (newVal.selectedAddress.eEan.ean !== this.props.selectedAddress.eEan.ean))
        ) {
            if (newVal.selectedAddress.p1Device) {
                this.setState({
                    'from': moment(),
                    'to': moment()
                }, () => {
                    try {
                        this.filter();
                    } catch (e) {
                        console.log(e);
                    }
                })
            } else {
                try {
                    this.filter();
                } catch (e) {
                    console.log(e);
                }
            }
        };
    }

    filter() {
        const {from, to} = this.state;
        this.props.getP4Usage({
            from: from.format('YYYYMMDD'),
            to: to.format('YYYYMMDD')
        });
        this.props.getMaxKwh({
            from: from.format('YYYYMMDD'),
            to: to.format('YYYYMMDD')
        });
    }

    onDateRangeChange(from, to) {
        this.setState({
            from,
            to
        }, this.filter);
        if (from.unix() !== to.unix()) {
            this.setState({
                showSun: false
            })
        }
    }

    toggleChartSettings() {
        this.setState({openChartSettings: !this.state.openChartSettings});
    }

    toggleItem (name) {
        let state = {};
        state[name] = !this.state[name]
        this.setState(state)
    }

    renderCustomChart() {
        const {isFetching, consumption} = this.props;
        const {from, to} = this.state;
        const toggleClass = cx({
            'btn-group': true,
            'open': this.state.openChartSettings
        });

        const toggleItem = this.toggleItem.bind(this);

        return (
            <div className="panel panel-primary">
                <div className="panel-heading">
                    <i className="fa fa-bar-chart-o fa-fw"/>
                    Elektriciteitsverbruik
                    <div className="pull-right">
                        <div className={toggleClass} >
                            <button type="button"
                                    className="btn btn-default btn-xs dropdown-toggle"
                                    data-toggle="dropdown"
                                    onClick={() => { this.toggleItem('openChartSettings') }}
                            >
                                Instellingen &nbsp;
                                <span className="caret" />
                            </button>
                            <ul className="dropdown-menu pull-right" role="menu">
                                <li>
                                    <a onClick={() => { toggleItem('showCosts') }}>
                                        {
                                            this.state.showCosts
                                                ? <i className="fa fa-check-square-o" aria-hidden="true"/>
                                                : <i className="fa fa-square-o" aria-hidden="true"/>
                                        }
                                        Toon kosten
                                    </a>
                                </li>
                                <li>
                                    <a onClick={() => { toggleItem('showTemperature') }}>
                                        {
                                            this.state.showTemperature
                                                ? <i className="fa fa-check-square-o" aria-hidden="true"/>
                                                : <i className="fa fa-square-o" aria-hidden="true"/>
                                        }
                                        Temperatuur
                                    </a>
                                </li>
                                <li>
                                    <a onClick={() => { toggleItem('showStandBy') }}>
                                        {
                                            this.state.showStandBy
                                                ? <i className="fa fa-check-square-o" aria-hidden="true"/>
                                                : <i className="fa fa-square-o" aria-hidden="true"/>
                                        }
                                        Sluimerverbruik
                                    </a>
                                </li>
                                <li>
                                    <a onClick={() => { toggleItem('showCO2') }}>
                                        {
                                            this.state.showCO2
                                                ? <i className="fa fa-check-square-o" aria-hidden="true"/>
                                                : <i className="fa fa-square-o" aria-hidden="true"/>
                                        }
                                        CO2
                                    </a>
                                </li>
                                { from.unix() === to.unix()
                                    ? <li>
                                        <a onClick={() => { toggleItem('showSun') }}>
                                            {
                                                this.state.showSun
                                                    ? <i className="fa fa-check-square-o" aria-hidden="true"/>
                                                    : <i className="fa fa-square-o" aria-hidden="true"/>
                                            }
                                            Toon daglicht
                                        </a>
                                    </li>
                                    : null
                                }
                            </ul>
                        </div>
                    </div>
                </div>
                <div className="panel-body">
                    <CustomChart preloader={isFetching}
                                 data={consumption}
                                 showSun={this.state.showSun}
                                 showStandBy={this.state.showStandBy}
                                 showTemperature={this.state.showTemperature}
                                 showCO2={this.state.showCO2}
                                 showCosts={this.state.showCosts}
                    />
                </div>
            </div>
        );
    }

    renderTotalPieChart() {
        const {isFetching, consumption} = this.props;
        return (
            <div className="panel panel-primary">
                <div className="panel-heading">
                    <i className="fa fa-bar-chart-o fa-fw"/>
                    Levering en teruglevering
                </div>
                <div className="panel-body">
                    <TotalPieChart
                        preloader={isFetching}
                        data={consumption}/>
                </div>
            </div>
        )
    }
    render() {
        const {isFetching, selectedAddress} = this.props;

        if (!selectedAddress) {
            return null;
        }

        const searchButtonClasses = cx({
            'b-button': true,
            'b-button--search': true,
            'b-button--disabled': isFetching
        });

        const {from, to} = this.state;
        const maxDate = selectedAddress.p1Device ? moment().endOf('day') : null;

        return (
        <div className="electricity">
            <div className="row">
                <div className="col-lg-12">
                    <h1 className="page-header">
                        <div className="row">
                            <div className="col-md-3">
                                Elektriciteit
                            </div>
                            <div className="col-md-6">
                                <div className="pull-left">
                                <DateRangePickerWrapper
                                    from={from}
                                    to={to}
                                    tabIndex={1}
                                    onDateRangeChange={this.onDateRangeChange}
                                    maxDate={maxDate}
                                />
                                </div>
                            </div>
                        </div>
                    </h1>

                </div>
            </div>
           <HeaderWidgets preloader={this.props.isFetching} data={this.props.consumption} maxKwh={this.props.maxKwh}/>
            <div className="row">
                <div className="col-md-12">
                    {this.renderCustomChart()}
                </div>
                <div className="col-md-12">
                    {this.renderTotalPieChart()}
                </div>

            </div>
        </div>
        );
    }
}

Electricity.propTypes = {
    isFetching: PropTypes.bool,
    consumption: PropTypes.object,
    maxKwh: PropTypes.object,
    getP4Usage: PropTypes.func.isRequired,
    getMaxKwh: PropTypes.func.isRequired,
    selectedAddress: PropTypes.object
};

export default Electricity;
