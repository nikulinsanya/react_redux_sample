import Electricity from './containers/ElectricityContainer';
import { injectReducer } from 'store/reducers';
import uiReducer from './reducers/ui/reducer';

export default (store) => ({
    path: 'electricity',
    getComponent(nextState, cb) {
        injectReducer(store, {key: 'electricityUI', reducer: uiReducer});
        cb(null, Electricity);
    }
});
