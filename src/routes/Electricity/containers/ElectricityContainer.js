import { connect } from 'react-redux';
import {
    getP4Usage,
    getMaxKwh
} from 'store/electricity/actions';

import Electricity from '../components/Electricity';

const mapDispatchToProps = {
    getP4Usage,
    getMaxKwh
};

const mapStateToProps = state => ({
    ...state.auth,
    ...state.electricity,
    ...state.electricityUI,
    ...state.currentUser
});

export default connect(mapStateToProps, mapDispatchToProps)(Electricity);
