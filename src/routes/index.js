// We only need to import the modules necessary for initial render
import CoreLayout from '../layouts/CoreLayout';
import Dashboard from './Dashboard';
import Electricity from './Electricity';
import Login from './Login';
const redirectToIndex = (nextState, replace) => replace('/dashboard');
export const createRoutes = (store) => ({
    path: '/',
    indexRoute: {onEnter: redirectToIndex},
    childRoutes: [
        {
            component: CoreLayout,
            childRoutes: [
                Dashboard(store),
                Electricity(store)
            ]
        },
        Login(store),
        {path: '*', onEnter: redirectToIndex}
    ]
});
export default createRoutes;
