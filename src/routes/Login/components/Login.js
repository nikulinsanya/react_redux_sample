import React, { Component } from 'react';
import {getConfig} from 'constants';
import PropTypes from 'prop-types';
import cx from 'classnames';
import NotificationsHolder from 'components/NotificationsHolder';

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            error: '',
            email: '',
            password: '',
            defaultRoot: '/dashboard',
            companyConfig: {}
        };
        this.login = this.login.bind(this);
        this.fillDemoInputs = this.fillDemoInputs.bind(this);
        this.changeEmail = e => this.changeValue('email', e.target.value);
        this.changePassword = e => this.changeValue('password', e.target.value);
    }

    componentDidMount() {
        this.props.clearUserState();
        const {company} = this.props;
    }

    componentWillMount() {
        const {token} = this.props;
        if (token) {
            this.context.router.push(this.state.defaultRoot);
        } else {
            this.props.logout();
        }
    }

    componentWillReceiveProps(newVal) {
        const {token, company} = newVal;
        if (token) {
            this.context.router.push(this.state.defaultRoot);
        }
    }

    login(e) {
        e.preventDefault();
        if (this.props.isPerformingLogin) {
            return false;
        }
        const {getToken} = this.props;
        const {email, password} = this.state;
        const loginData = {
            userName: email,
            password: password
        };
        getToken(loginData);
    }

    fillDemoInputs(e) {
        const {company} = this.props;
        const companyConfig = getConfig(company);
        this.setState({
            email: companyConfig.demoCredentials.email,
            password: companyConfig.demoCredentials.password
        }, () => {
            this.login(e);
        });
    }

    changeValue(field, value) {
        this.props.clearLoginError();

        this.setState({
            [field]: value
        });
    }

    render() {
        const {loginError, isPerformingLogin, company, notifications, hideNotification} = this.props;
        const {email, password} = this.state;
        const saveButtonClasses = cx({
            'btn btn-primary': true,
            'button-loading': isPerformingLogin
        });

        const companyConfig = getConfig(company);
        const loginContainerClass = cx({
            'login-container': true,
            [`company-${companyConfig && companyConfig.mainClass}`]: true
        });

        return (
            <div className={loginContainerClass}>
                <NotificationsHolder
                    hideNotification={hideNotification}
                    notifications={notifications}/>
                <div className="banner-background-image" />
                <section id="banner">
                    <div className="inner">
                        <h2>Sanya's React + Redux sample</h2>
                        <p >INLOGGEN</p>

                    <form className="login-form container-fluid regular-form"
                          onSubmit={this.login}
                          method="post"
                          role="form">
                        <span className="top-error-message">{loginError}</span>
                        <ul className="actions">
                            <li className="with-input">
                                <input
                                    type="email"
                                    name="email"
                                    className="form-control"
                                    placeholder="email"
                                    onChange={this.changeEmail}
                                    value={email} required autoFocus/>
                            </li>
                            <li className="with-input">
                                <input
                                    type="password"
                                    name="password"
                                    className="form-control"
                                    placeholder="Wachtwoord"
                                    onChange={this.changePassword}
                                    value={password}
                                    required/>
                            </li>
                            <li>
                                <button
                                    type="submit"
                                    className={saveButtonClasses}>
                                    <img
                                        className="loader"
                                        src="./img/loader-button.gif"
                                        alt="loading"/>
                                    Log in
                                </button>
                            </li>
                        </ul>

                        <ul className="actions">
                            <li >
                                <button className="btn btn-primary special" type="button" onClick={this.fillDemoInputs}>
                                    <span>DEMO ACCOUNT</span>
                                </button>
                            </li>
                        </ul>
                    </form>
                    </div>
                    <span className="more scrolly">
                        <span >Geen account? </span>
                        <a href="#/register" >Aanmelden</a>
                        <br/><br/>
                        <span className="pull-left">Wachtwoord vergeten? &nbsp;</span>
                         <a className="btn-forgot-password text-uppercase ng-binding"
                            href="#/reset">
                             Resetten
                         </a>
                    </span>
                </section>

            </div>

        );
    }
}

Login.contextTypes = {
    router: PropTypes.object
};

Login.propTypes = {
    getToken: PropTypes.func.isRequired,
    logout: PropTypes.func.isRequired,
    token: PropTypes.string,
    loginError: PropTypes.string,
    clearLoginError: PropTypes.func.isRequired,
    isPerformingLogin: PropTypes.bool,
    company: PropTypes.string,
    clearUserState: PropTypes.func.isRequired,
    notifications: PropTypes.array,
    hideNotification: PropTypes.func.isRequired,

};

export default Login;
