import {
    LOGIN_REQUEST,
    LOGIN_FAILURE,
    LOGIN_SUCCESS,
    CONFIRM_EMAIL_REQUEST,
    CONFIRM_EMAIL_SUCCESS,
    CONFIRM_EMAIL_FAILURE
} from 'store/auth/constants';

const ACTION_HANDLERS = {
    [LOGIN_REQUEST]: state => {
        return {
            ...state,
            isPerformingLogin: true
        };
    },
    [LOGIN_FAILURE]: state => {
        return {
            ...state,
            isPerformingLogin: false
        };
    },
    [LOGIN_SUCCESS]: state => {
        return {
            ...state,
            isPerformingLogin: false
        };
    },
    [CONFIRM_EMAIL_REQUEST]: state => {
        return {
            ...state,
            confirmEmailFetch: true
        };
    },
    [CONFIRM_EMAIL_SUCCESS]: state => {
        return {
            ...state,
            confirmEmailFetch: false
        };
    },
    [CONFIRM_EMAIL_FAILURE]: state => {
        return {
            ...state,
            confirmEmailFetch: false
        };
    }
};

const initialState = {
    isPerformingLogin: false,
    confirmEmailFetch: false
};

export default function loginUI(state = initialState, action) {
    const handler = ACTION_HANDLERS[action.type];

    return handler ? handler(state, action) : state;
}
