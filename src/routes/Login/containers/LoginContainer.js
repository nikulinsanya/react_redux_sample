import { connect } from 'react-redux';
import {
    getToken,
    logout,
    clearLoginError,
} from 'store/auth/actions';
import {
    showNotification,
    hideNotification
} from 'store/notifications/actions';

import {
    clearUserState
} from 'store/currentUser/actions'

import Login from '../components/Login';

const mapDispatchToProps = {
    getToken,
    logout,
    clearLoginError,
    hideNotification,
    showNotification,
    clearUserState
};

const mapStateToProps = (state) => ({
    ...state.auth,
    ...state.notifications,
    ...state.loginUI
});

export default connect(mapStateToProps, mapDispatchToProps)(Login);
