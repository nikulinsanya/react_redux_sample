import Login from './containers/LoginContainer';
import {injectReducer} from 'store/reducers';
import loginUIReducer from './reducers/ui/reducer';

export default (store) => ({
    path: 'login',
    getComponent(nextState, cb) {
        injectReducer(store, {key: 'loginUI', reducer: loginUIReducer});
        cb(null, Login);
    }
});
