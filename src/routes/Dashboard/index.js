import Dashboard from './containers/DashboardContainer';
import { injectReducer } from 'store/reducers';
import uiReducer from './reducers/ui/reducer';
export default (store) => ({
    path: 'dashboard',
    getComponent(nextState, cb) {
        injectReducer(store, {key: 'dashboardUI', reducer: uiReducer});
        cb(null, Dashboard);
    }
});
