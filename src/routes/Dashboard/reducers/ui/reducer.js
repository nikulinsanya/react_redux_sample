import {
    EL_DASHBOARD_DATA_REQUEST,
    EL_DASHBOARD_DATA_SUCCESS,
    EL_DASHBOARD_DATA_FAILURE,
    GAS_DASHBOARD_DATA_REQUEST,
    GAS_DASHBOARD_DATA_SUCCESS,
    GAS_DASHBOARD_DATA_FAILURE
} from 'store/dashboard/constants';

const ACTION_HANDLERS = {
    [EL_DASHBOARD_DATA_REQUEST]: (state, action) => {
        const {data} = action;
        return {
            ...state,
            isFetching: true
        };
    },
    [EL_DASHBOARD_DATA_SUCCESS]: (state, action) => {
        const {data} = action;
        return {
            ...state,
            isFetching: false
        };
    },
    [EL_DASHBOARD_DATA_FAILURE]: (state, action) => {
        const {data} = action;
        return {
            ...state,
            isFetching: false
        };
    }
};

const initialState = {
    isFetching: true
};

export default function uiReducer(state = initialState, action) {
    const {type} = action;
    const handler = ACTION_HANDLERS[type];
    return handler ? handler(state, action) : state;
};
