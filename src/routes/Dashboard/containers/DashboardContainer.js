import { connect } from 'react-redux';
import {
    getElLasDay,
    getGasLasDay,
    getRTLast,
    clearState
} from 'store/dashboard/actions';
import { getMaxKwh } from 'store/electricity/actions';

import Dashboard from '../components/Dashboard';

const mapDispatchToProps = {
    getElLasDay,
    getGasLasDay,
    getRTLast,
    clearState,
    getMaxKwh
};

const mapStateToProps = state => ({
    ...state.auth,
    ...state.dashboard,
    ...state.dashboardUI,
    ...state.electricity,
    ...state.currentUser
});

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);
