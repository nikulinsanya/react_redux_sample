import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import LastDayElectricity from './widgets/LastDayElectricity';
import LastDayGas from './widgets/LastDayGas';
import LastDayProduction from './widgets/LastDayProduction';
import LastDayStandby from './widgets/LastDayStandby';
import LimitProgress from './widgets/LimitProgress';
import LastDayElectricityPieChart from './widgets/LastDayElectricityPieChart'
import RealtimeLineChart from './widgets/RealtimeLineChart'
import LastDayNeighboursEl from './widgets/LastDayNeighboursEl';
import LastDayNeighboursGas from './widgets/LastDayNeighboursGas';
import CurrentElectrivityGaugeChart from './widgets/CurrentElectrivityGaugeChart';
import CurrentGasGaugeChart from './widgets/CurrentGasGaugeChart';
import Weather from './widgets/Weather';
import WidgetPreloader from 'components/WidgetPreloader';
import LastDayDemand from './widgets/LastDayDemand';
import LastDayGradient from './widgets/lastDayGradient';
import LastDayGasGradient from './widgets/LastDayGasGradient'
import MaxKwh from './widgets/MaxKwh'

class Dashboard extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            searchString: '',
            loaded: false,
            from: moment().add(-2, 'day')
        };
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.selectedAddress && nextProps.selectedAddress.id !== this.props.selectedAddress.id) {
            this.setState({
                loaded: false
            }, () => {
                this.setState({
                    ean: nextProps.selectedAddress.eEan && nextProps.selectedAddress.eEan.ean,
                    gean: nextProps.selectedAddress.gEan && nextProps.selectedAddress.gEan.ean
                }, this.filter)
            })
            return false;
        }
        if ((nextProps.selectedAddress && !this.state.loaded)) {
            this.setState({
                ean: nextProps.selectedAddress.eEan && nextProps.selectedAddress.eEan.ean,
                gean: nextProps.selectedAddress.gEan && nextProps.selectedAddress.gEan.ean
            }, this.filter)
        }
        if (nextProps.elConsumption) {
            this.setState({
                elConsumption: nextProps.elConsumption
            })
        }
        if (nextProps.dashboardGasConsumption) {
            this.setState({
                dashboardGasConsumption: nextProps.dashboardGasConsumption
            })
        }
    }

    setDate(direction) {
        let from = this.state.from;
        this.setState({
            from: from.clone().add(direction > 0 ? 1 : -1, 'days')
        }, this.filter)
    }

    filter() {
        const {user} = this.props;
        const { ean, gean, from } = this.state;
        const isDemoCustomer = user && user.isDemoCustomer;
        this.setState({
            loaded: true
        });

        if (ean) {
            this.props.getElLasDay(ean, isDemoCustomer, from.format('YYYYMMDD'));
            this.props.getMaxKwh({
                from: from.clone().format('YYYYMMDD'),
                to: from.format('YYYYMMDD')
            });
        }
        if (gean) {
            this.props.getGasLasDay(gean, isDemoCustomer, from.format('YYYYMMDD'));
        }
    }

    renderNoData() {
        return (
            <div className="dashboard">
                <div className="row">
                    <div className="col-lg-12">
                        <h4 className="page-header">
                            <div className="row">
                                <div className="col-md-12 col-sm-12">
                                    Om app te gebruiken, moet u een locatie toevoegen
                                    &nbsp;
                                    &nbsp;
                                    <a className="btn btn-primary" href="#/profile/addaddress" >
                                        <i className="fa fa-home" />
                                        &nbsp;Voeg locatie toe
                                    </a>
                                </div>
                                <div className="clearfix" />
                            </div>
                        </h4>

                    </div>
                </div>
            </div>
        )
    }

    render() {
        const {user, rtConsumption, maxKwh, maxM3} = this.props;
        const hisItems = user && user.zabbixHosts && user.zabbixHosts[0] && user.zabbixHosts[0].hisItems;
        const isDemoCustomer = user && user.isDemoCustomer;
        const {selectedAddress} = this.props;
        const {elConsumption, dashboardGasConsumption, loaded, from} = this.state;
        const address = selectedAddress
            ? (selectedAddress.street + ' ' + selectedAddress.buildingNr + (selectedAddress.buildingNrEx || ''))
            : null;
        const date = from.format('DD MMM YYYY');
        const isTomorrowAllowed = from.startOf('day').isBefore(moment().add(-1, 'day').startOf('day'))

        const hasGas = selectedAddress && selectedAddress.gEan;
        const hasEl = selectedAddress && selectedAddress.eEan;
        const hasElProd = elConsumption &&
            (elConsumption.totalUsage &&
            (elConsumption.totalUsage.r281 || elConsumption.totalUsage.r282)) ||
            (elConsumption && elConsumption.totalUsagePrev &&
                (elConsumption.totalUsagePrev.r281 || elConsumption.totalUsagePrev.r282));
        if (!address) {
            return this.renderNoData()
        }
        if (!loaded) {
            return <WidgetPreloader/>
        }

        return (
        <div className="dashboard">
            <div className="row">
                <div className="col-lg-12">
                    <h1 className="page-header">
                        <div className="row">
                            <div className="col-md-12 col-sm-12">
                                {address}
                                <div className="pull-right">
                                    <a onClick={() => { this.setDate(-1) }}>
                                        <span className="fa fa-chevron-left" />
                                    </a>
                                    {date}
                                    {isTomorrowAllowed
                                        ? <a onClick={() => { this.setDate(1) }}>
                                            <span className="fa fa-chevron-right" />
                                        </a>
                                        : null}
                                </div>
                            </div>
                            <div className="clearfix" />
                        </div>
                    </h1>

                </div>
            </div>
            <div className="row widgets">
                {isDemoCustomer
                ? <div className="col-lg-8 col-md-8 col-sm-12">
                    <RealtimeLineChart/>
                </div>
                : null}
                {hasEl
                ? <div className="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                    <LastDayElectricity data={elConsumption} addressId={selectedAddress.id}/>
                </div>
                    : null
                }
                {hasEl && hasElProd
                    ? <div className="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                    <LastDayProduction data={elConsumption} addressId={selectedAddress.id}/>
                    </div>
                    : null
                }
                {maxKwh
                    ? <div className="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                        <MaxKwh maxKwh={maxKwh}/>
                    </div>
                    : null
                }

                {/* { rtConsumption */}
                    {/* ? <div className="col-lg-4 col-md-6 col-sm-6 col-xs-12 "> */}
                        {/* <DayPieChart data={rtConsumption} items={hisItems}/> */}
                    {/* </div> */}
                    {/* : null} */}

                {isDemoCustomer
                ? <div className="col-lg-4 col-md-6 col-sm-6 col-xs-12 ">
                    <CurrentElectrivityGaugeChart/>
                </div>
                : null}
                {isDemoCustomer
                ? <div className="col-lg-4 col-md-6 col-sm-6 col-xs-12 ">
                    <CurrentGasGaugeChart/>
                </div>
                : null}

                {hasGas
                ? <div className="col-lg-4 col-md-6 col-sm-6 col-xs-12 ">
                    <LastDayGas data={dashboardGasConsumption} addressId={selectedAddress.id} maxM3={maxM3}/>
                </div>
                : null
                }
                {isDemoCustomer
                ? <div className="col-lg-4 col-md-6 col-sm-6 col-xs-12 ">
                    <LastDayStandby data={elConsumption}/>
                </div>
                : null}
                {isDemoCustomer
                ? <div className="col-lg-4 col-md-6 col-sm-6 col-xs-12 ">
                    <LimitProgress/>
                </div>
                : null}
                {isDemoCustomer
                ? <div className="col-lg-4 col-md-6 col-sm-6 col-xs-12 ">
                    <LastDayDemand/>
                </div>
                : null}
                <div className="col-lg-4 col-md-6 col-sm-6 col-xs-12 ">
                    <LastDayElectricityPieChart data={elConsumption}/>
                </div>
                {isDemoCustomer
                ? <div className="col-lg-4 col-md-6 col-sm-6 col-xs-12 ">
                    <LastDayNeighboursEl data={elConsumption}/>
                </div>
                : null}
                {isDemoCustomer
                ? <div className="col-lg-4 col-md-6 col-sm-6 col-xs-12 ">
                    <LastDayNeighboursGas data={elConsumption}/>
                </div>
                : null}
                {isDemoCustomer
                ? <div className="col-lg-4 col-md-6 col-sm-6 col-xs-12 ">
                    <Weather/>
                </div>
                : null}
                {isDemoCustomer
                    ? <div className="col-lg-4 col-md-6 col-sm-6 col-xs-12 ">
                        <LastDayGradient/>
                    </div>
                    : null}
                {isDemoCustomer
                    ? <div className="col-lg-4 col-md-6 col-sm-6 col-xs-12 ">
                        <LastDayGasGradient/>
                    </div>
                    : null}

                {/* { rtConsumption */}
                {/* ? <div className="col-lg-12 col-md-12 col-sm-12"> */}
                        {/* <DayLineChart data={rtConsumption} items={hisItems}/> */}
                    {/* </div> */}
                {/* : null} */}
            </div>
        </div>
        );
    }
}

Dashboard.propTypes = {
    getElLasDay: PropTypes.func.isRequired,
    getGasLasDay: PropTypes.func.isRequired,
    getRTLast: PropTypes.func.isRequired,
    dashboardGasConsumption: PropTypes.object,
    elConsumption: PropTypes.object,
    selectedAddress: PropTypes.object,
    isFetching: PropTypes.bool,
    clearState: PropTypes.func.isRequired,
    user: PropTypes.object,
    getMaxKwh: PropTypes.func.isRequired,
    maxKwh: PropTypes.object,
    maxM3: PropTypes.object
};

export default Dashboard;
