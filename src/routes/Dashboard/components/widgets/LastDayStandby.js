import React from 'react';
import PropTypes from 'prop-types';
import WidgetPreloader from 'components/WidgetPreloader';

class LastDayStandby extends React.Component {
    render() {
        const { data } = this.props;
        return (
            <div className="panel panel-primary dashboard__widget">
                <div className="panel-heading">
                    Sluimerverbruik
                </div>
                <div className="panel-heading panel-heading--light">
                    <div className="row">
                        <div className="col-xs-5 left-icon">
                            <i className="fa fa-clock-o fa-5x"/>
                            <div>
                                -15 %
                                <i className="fa fa-caret-down" aria-hidden="true"/>
                            </div>
                        </div>
                        <div className="col-xs-7 text-right">
                            <div className="big-number">
                                2 kWh
                            </div>
                            <div className="big-number__sub">
                                <br/>
                                22% of total
                            </div>
                        </div>
                    </div>
                </div>
                <div className="panel-footer">
                        <span className="pull-right">
                            Kosten sluimerverbruik gisteren € 4.43
                        </span>
                    <div className="clearfix" />
                </div>
            </div>
        );
    }
}

LastDayStandby.propTypes = {
    data: PropTypes.object
};

export default LastDayStandby;
