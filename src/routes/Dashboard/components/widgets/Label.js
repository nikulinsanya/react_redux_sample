import React from 'react';
import PropTypes from 'prop-types';
import WidgetPreloader from 'components/WidgetPreloader';

class Label extends React.Component {
    render() {
        const { data } = this.props;
        return (
            <div className="panel panel-primary dashboard__widget">
                <div className="panel-heading">
                    Uw energielabel
                </div>
                <div className="panel-heading panel-heading--light">
                    <div className="row">
                        <div className="col-sm-12 left-icon">
                            <img src="/img/energy-labels/b.jpg" className="energy-label"/>
                        </div>

                    </div>
                </div>
                <div className="panel-footer">
                        <span className="pull-right">
                            B Energielabel
                        </span>
                    <div className="clearfix" />
                </div>
            </div>
        );
    }
}

Label.propTypes = {
    data: PropTypes.object
};

export default Label;
