import React from 'react';
import PropTypes from 'prop-types';
import WidgetPreloader from 'components/WidgetPreloader';
import cx from 'classnames';

class LastDayElectricity extends React.Component {
    render() {
        const { data } = this.props;

        if (!data) {
            return <WidgetPreloader/>
        }

        const { totalUsage, totalCost, totalUsagePrev } = data;
        const totalU = totalUsage && (totalUsage.r181 + totalUsage.r182) || 0;
        const prevU = totalUsagePrev && (totalUsagePrev.r181 + totalUsagePrev.r182) || 0;
        const trend = prevU && totalU ? (100 - (100 * prevU / totalU)).toLocaleString('nl-NL', { maximumFractionDigits: 1 }) : 0;
        const cost = totalCost && totalCost.r181 + totalCost.r181 || 0;
        const trendClass = cx({
            'panel panel-primary dashboard__widget': true,
            'red': trend > 0
        })
        const editLink = `#/profile/editaddress/${this.props.addressId}`
        return (
            <div className={trendClass}>
                <div className="panel-heading">
                    Electriciteits afname
                </div>
                <div className="panel-heading panel-heading--light">
                    <div className="row">
                        <div className="col-xs-5 left-icon">
                            <i className="fa fa-flash fa-5x"/>
                            <div>
                                {trend} %
                                {
                                    trend > 0
                                        ? <i className="fa fa-caret-up" aria-hidden="true"/>
                                        : <i className="fa fa-caret-down" aria-hidden="true"/>
                                }
                            </div>
                        </div>
                        <div className="col-xs-7 text-right">
                            <div className="big-number">
                                {totalU.toLocaleString('nl-NL', { maximumFractionDigits: 2 })}  kWh
                            </div>
                            <div className="big-number__sub">
                                Laag: {totalUsage && totalUsage.r181 && totalUsage.r181.toLocaleString('nl-NL', { maximumFractionDigits: 2 }) || 0}  kWh
                                <br/>
                                Hoog: {totalUsage && totalUsage.r182 && totalUsage.r182.toLocaleString('nl-NL', { maximumFractionDigits: 2 }) || 0} kWh
                            </div>
                        </div>
                    </div>
                </div>
                <div className="panel-footer">
                        <span className="pull-right" >
                            Kosten:  € {cost.toLocaleString('nl-NL', { maximumFractionDigits: 2 })}
                            <a href={editLink} className="edit-cost">
                                <i className="fa fa-pencil" />
                            </a>
                        </span>
                    <div className="clearfix" />
                </div>
            </div>
        );
    }
}

LastDayElectricity.propTypes = {
    data: PropTypes.object,
    addressId: PropTypes.number
};

export default LastDayElectricity;
