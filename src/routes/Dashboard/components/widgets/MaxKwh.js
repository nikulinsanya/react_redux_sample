import React from 'react';
import PropTypes from 'prop-types';
import WidgetPreloader from 'components/WidgetPreloader';
import cx from 'classnames';
import moment from 'moment';

class MaxKwh extends React.Component {
    render() {
        const {maxKwh} = this.props;
        if (!maxKwh) {
            return null;
        }
        const r180Max = maxKwh.r180.usage * 4;
        const r180Date = moment(maxKwh.r180.momentFrom, 'YYYYMMDD HH:mm').format('DD MMM YYYY HH:mm') +
            ' - ' +
            moment(maxKwh.r180.momentTo, 'YYYYMMDD HH:mm').format('HH:mm');

        const r280Max = maxKwh.r280.usage * 4;
        const r280Date = moment(maxKwh.r280.momentFrom, 'YYYYMMDD HH:mm').format('DD MMM YYYY HH:mm') +
            ' - ' +
            moment(maxKwh.r280.momentTo, 'YYYYMMDD HH:mm').format('HH:mm');

        return (
            r180Max
                ? <div className="panel panel-primary">
                    <div className="panel-heading">
                        Max Watt
                    </div>
                    <div className="panel-heading panel-heading--light max-kwh-heading">
                        <div className="row">
                            <div className="col-xs-12 max-kwh">
                                <span className="max-kwh-title">Elektriciteitsverbruik:</span>
                                <span className="max-kwh-value">
                                    {r180Max.toLocaleString('nl-NL', { maximumFractionDigits: 1 })} Watt
                                </span>
                                <span className="max-kwh-date">
                                    <i className="fa fa-clock-o mr-2" />
                                    {r180Date}
                                </span>
                            </div>
                            {r280Max
                                ? <div className="col-xs-12 max-kwh">
                                    <span className="max-kwh-title">Terug levering:</span>
                                    <span className="max-kwh-value">
                                        {r280Max.toLocaleString('nl-NL', { maximumFractionDigits: 1 })} Watt
                                    </span>
                                    <span className="max-kwh-date">
                                        <i className="fa fa-clock-o mr-2" />
                                        {r280Date}
                                    </span>
                                </div>
                                : null
                                }
                        </div>
                    </div>
                    {/* <div className="panel-footer"> */}
                            {/* <span className="pull-right"> */}
                                {/* <i className="fa fa-clock-o mr-2"></i> */}
                                {/* {r180Date}<br/> */}
                                {/* </span> */}
                        {/* <div className="clearfix" /> */}
                    {/* </div> */}
                </div>
                : <div className="panel panel-primary">
                    <div className="panel-heading">
                        Elektriciteitsverbruik: Max Watt
                    </div>
                    <div className="panel-heading padding-30-px">
                        <div className="row">
                            <div className="col-xs-3">
                                <i className="fa fa-exclamation-triangle fa-5x" aria-hidden="true" />
                            </div>
                            <div className="col-xs-9 text-right">
                                <div>
                                    Geen data
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="panel-footer">
                        <span className="pull-right" />
                        <div className="clearfix" />
                    </div>
                </div>
        )
    }
}

MaxKwh.propTypes = {
    maxKwh: PropTypes.object
};

export default MaxKwh;
