import React from 'react';
import PropTypes from 'prop-types';
var ReactHighcharts = require('react-highcharts');

class RealtimePieChart extends React.Component {
    constructor(props) {
        super(props);
        this.state = {

        };
    }

    componentDidMount() {
        this.setState({
            config:{
                chart: {
                    type: 'pie',
                    animation: ReactHighcharts.svg, // don't animate in old IE
                    height: 180
                },
                credits: {
                    enabled: false
                },
                title: {
                    text: this.props.title
                },
                series: [{
                    innerSize: '30%',
                    data: [
                        {name: 'Floor 1', y: Math.abs((Math.random() * 10).toLocaleString('nl-NL', { maximumFractionDigits: 2 }))},
                        {name: 'Floor 2', y: Math.abs((Math.random() * 10).toLocaleString('nl-NL', { maximumFractionDigits: 2 }))},
                        {name: 'Floor 3', y: Math.abs((Math.random() * 10).toLocaleString('nl-NL', { maximumFractionDigits: 2 }))}
                    ]
                }]
            }
        }, () => {
            const self = this;
            let interval =
                setInterval(() => {
                    let chart = self.refs.chart.getChart();
                    chart.reflow();
                    const y = Math.abs((Math.random() * 10).toLocaleString('nl-NL', { maximumFractionDigits: 2 }));

                    if (chart.series) {
                        chart.series[0].setData([
                            Math.abs((Math.random() * 10).toLocaleString('nl-NL', { maximumFractionDigits: 2 })),
                            Math.abs((Math.random() * 10).toLocaleString('nl-NL', { maximumFractionDigits: 2 })),
                            Math.abs((Math.random() * 10).toLocaleString('nl-NL', { maximumFractionDigits: 2 }))
                        ]);
                        chart.reflow();
                    }
                }, 2000);
            this.setState({intervalId: interval});
        })
    }

    componentWillUnmount () {
        clearInterval(this.state.intervalId);
    }

    render() {
        return (
            this.state.config
            ? <ReactHighcharts config={this.state.config} ref="chart" />
            : null

        );
    }
}

RealtimePieChart.propTypes = {
    title: PropTypes.string
};

export default RealtimePieChart;
