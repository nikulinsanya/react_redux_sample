import React from 'react';
import PropTypes from 'prop-types';
import WidgetPreloader from 'components/WidgetPreloader';
var ReactHighcharts = require('react-highcharts');

class LastDayNeighboursEl extends React.Component {
    constructor(props) {
        super(props);
        this.state = {

        };
    }

    prepareData(consumption, production) {
        return {
            chart: {
                type: 'column',
                animation: ReactHighcharts.svg, // don't animate in old IE
                height: 130,
                backgroundColor: '#fff'
            },
            xAxis: {
                categories: [
                    'Uw verbruik',
                    'Buurtgenoten verbruik'
                ]
            },
            yAxis: {
                min: 0,
                title: {
                    text: ''
                }
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.y:.1f}kWh</b>'
            },
            credits: {
                enabled: false
            },
            title: {
                text: ''
            },
            plotOptions:{
                column: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    }
                }
            },
            series: [
                {
                    name: 'Consumptie',
                    showInLegend: false,
                    data: [
                        {
                            y: Math.abs(consumption),
                            color: 'rgba(244, 217, 66,1)'
                        },
                        {
                            y: Math.abs(production),
                            color: '#00a500'
                        }
                    ],
                    color: 'rgba(244, 217, 66,1)'
                }
            ]
        }
    }

    render() {
        const { data } = this.props;
        if (!data) {
            return <WidgetPreloader/>
        }

        const { totalUsage, totalCost, totalUsagePrev } = data;
        const totalU = totalUsage && (totalUsage.r181 + totalUsage.r182) || 0;
        const totalP = totalUsage && (totalUsage.r281 + totalUsage.r282) || 0;
        const prevU = totalUsagePrev && (totalUsagePrev.r181 + totalUsagePrev.r182) || 0;

        const config = this.prepareData(totalU, totalP);
        return (
            config
            ? <div className="panel panel-primary dashboard__widget">
                    <div className="panel-heading">
                        Vergelijk elektriciteit met buurtgenoten
                    </div>
                    <div className="panel-heading chart-100">
                        <ReactHighcharts config={config} ref="chart" />
                    </div>
                    <div className="panel-footer">
                        <span className="pull-right">
                            {totalU.toLocaleString('nl-NL', { maximumFractionDigits: 2 })} kWh/ {totalP.toLocaleString('nl-NL', { maximumFractionDigits: 2 })} kWh
                        </span>
                        <div className="clearfix" />
                    </div>
                </div>

            : null

        );
    }
}

LastDayNeighboursEl.propTypes = {
    data: PropTypes.object
};

export default LastDayNeighboursEl;
