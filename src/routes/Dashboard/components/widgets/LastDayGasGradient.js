import React from 'react';
import PropTypes from 'prop-types';
import WidgetPreloader from 'components/WidgetPreloader';

class LastDayGasGradient extends React.Component {
    render() {
        const { data } = this.props;
        const arrowPosStyle = { left: '20%', color: '#cb4d00' };
        return (
            <div className="panel panel-primary dashboard__widget">
                <div className="panel-heading">
                    Je gasverbruik vergeleken
                </div>
                <div className="panel-heading panel-heading--light">
                    <div className="row">
                        <div className="col-md-12">
                            <div className="gradient">
                                <i className="fa fa-arrow-down gradient-pointer"
                                   style={arrowPosStyle}
                                   aria-hidden="true" />
                                <div className="gradient-progress" />
                                <div className="gradient-label-left">
                                    30 m3/m2
                                </div>
                                <div className="gradient-label-right">
                                    3 m3/m2
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="panel-footer">
                        <span className="pull-right">
                            Ten opzichte van je sector afgelopen 30 dagen
                        </span>
                    <div className="clearfix" />
                </div>
            </div>
        );
    }
}

LastDayGasGradient.propTypes = {
    data: PropTypes.object
};

export default LastDayGasGradient;
