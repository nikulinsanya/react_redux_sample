import React from 'react';
import PropTypes from 'prop-types';
import WidgetPreloader from 'components/WidgetPreloader';
import cx from 'classnames';
import moment from 'moment';

class LastDayGas extends React.Component {
    render() {
        const { data } = this.props;
        if (!data) {
            return (
                <div className="panel panel-primary dashboard__widget">
                    <div className="panel-heading">
                        Gasverbruik: Gisteren
                    </div>
                    <div className="panel-heading panel-heading--light">
                        <div className="row">
                            <div className="col-xs-12 left-icon">
                                <i className="fa fa-fire fa-5x"/>
                                <div>
                                   Geen data
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="panel-footer">
                        <span className="pull-right">
                            Geen data
                            <a href={editLink} className="edit-cost">
                                <i className="fa fa-pencil" />
                            </a>
                        </span>
                        <div className="clearfix" />
                    </div>
                </div>
            )
        }
        const {maxM3} = this.props;

        const r180Max = maxM3 ? maxM3.r180.usage : false
        const r180Date = maxM3 ? (moment(maxM3.r180.momentFrom, 'YYYYMMDD HH:mm').format('DD MMM YYYY HH:mm') +
            ' - ' +
            moment(maxM3.r180.momentTo, 'YYYYMMDD HH:mm').format('HH:mm')) : false;
        const { totalUsage, totalCost, totalUsagePrev } = data;
        const total = totalUsage && (totalUsage.r180) || 0;
        const prev = totalUsagePrev && (totalUsagePrev.r180) || 0;
        const trend = total && prev ? (100 - (100 * prev / total)).toLocaleString('nl-NL', { maximumFractionDigits: 1 }) : 0;
        const cost = totalCost && totalCost.r180 || 0;
        const trendClass = cx({
            'panel panel-primary dashboard__widget': true,
            'red': trend > 0
        });
        const editLink = `#/profile/editaddress/${this.props.addressId}`
        const maxm3Style = {
            'marginTop': '-5px'
        }

        return (
            <div className={trendClass}>
                <div className="panel-heading">
                    Gasverbruik: Gisteren
                </div>
                <div className="panel-heading panel-heading--light">
                    <div className="row">
                        <div className="col-xs-5 left-icon">
                            <i className="fa fa-fire fa-5x"/>
                            <div>
                                {trend} %
                                {
                                    trend > 0
                                        ? <i className="fa fa-caret-up" aria-hidden="true"/>
                                        : <i className="fa fa-caret-down" aria-hidden="true"/>
                                }
                            </div>
                        </div>
                        <div className="col-xs-7 text-right">
                            <div className="big-number">
                                {total.toLocaleString('nl-NL', { maximumFractionDigits: 1 })}  m3
                            </div>
                            <div className="big-number__sub">
                                prev day: {prev.toLocaleString('nl-NL', { maximumFractionDigits: 1 })} m3
                            </div>
                            <div className="big-number__sub" style={maxm3Style}>
                                max m3: {r180Max.toLocaleString('nl-NL', { maximumFractionDigits: 1 })} m3
                                <br/>
                                <i className="fa fa-clock-o mr-2" />
                                {r180Date}
                            </div>

                        </div>
                    </div>
                </div>
                <div className="panel-footer">
                        <span className="pull-right">
                            Kosten: € {cost.toLocaleString('nl-NL', { maximumFractionDigits: 2 })}
                            <a href={editLink} className="edit-cost">
                                <i className="fa fa-pencil" />
                            </a>
                        </span>
                    <div className="clearfix" />
                </div>
            </div>
        );
    }
}

LastDayGas.propTypes = {
    data: PropTypes.object,
    maxM3: PropTypes.object,
    addressId: PropTypes.number
};

export default LastDayGas;
