import React from 'react';
import PropTypes from 'prop-types';
import ReactHighcharts from 'react-highcharts';
import HighchartsMore from 'highcharts-more';
import SolidGauge from 'highcharts-solid-gauge';

HighchartsMore(ReactHighcharts.Highcharts);
SolidGauge(ReactHighcharts.Highcharts);

class RealtimeGaugeChart extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            intervalId: false
        };
        // console.log(ReactHighcharts.Highcharts,SolidGauge(ReactHighcharts.Highcharts));
        // debugger;
        // SolidGauge(ReactHighcharts.Highcharts);
    }

    componentWillUnmount () {
        clearInterval(this.state.intervalId);
    }

    componentDidMount() {
        this.setState({
            config: {
                chart: {
                    type: 'gauge',
                    plotBackgroundColor: null,
                    plotBackgroundImage: null,
                    plotBorderWidth: 0,
                    plotShadow: false
                },
                credits: {
                    enabled: false
                },

                title: {
                    text: this.props.title
                },

                pane: {
                    startAngle: -150,
                    endAngle: 150,
                    background: [{
                        backgroundColor: {
                            linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
                            stops: [
                                [0, '#FFF'],
                                [1, '#333']
                            ]
                        },
                        borderWidth: 0,
                        outerRadius: '109%'
                    }, {
                        backgroundColor: {
                            linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
                            stops: [
                                [0, '#333'],
                                [1, '#FFF']
                            ]
                        },
                        borderWidth: 1,
                        outerRadius: '107%'
                    }, {
                        // default background
                    }, {
                        backgroundColor: '#DDD',
                        borderWidth: 0,
                        outerRadius: '105%',
                        innerRadius: '103%'
                    }]
                },

                // the value axis
                yAxis: {
                    min: 0,
                    max: 16,

                    minorTickInterval: 'auto',
                    minorTickWidth: 1,
                    minorTickLength: 10,
                    minorTickPosition: 'inside',
                    minorTickColor: '#666',

                    tickPixelInterval: 30,
                    tickWidth: 2,
                    tickPosition: 'inside',
                    tickLength: 10,
                    tickColor: '#666',
                    labels: {
                        step: 2,
                        rotation: 'auto'
                    },
                    title: {
                        text: 'kWh'
                    },
                    plotBands: [{
                        from: 0,
                        to: 2,
                        color: '#55BF3B' // green
                    }, {
                        from: 2,
                        to: 10,
                        color: '#DDDF0D' // yellow
                    }, {
                        from: 10,
                        to: 16,
                        color: '#DF5353' // red
                    }]
                },

                series: [{
                    name: 'Consumption at ' + this.props.title,
                    data: [Math.abs((Math.random() - 0.5) * 10)],
                    tooltip: {
                        valueSuffix: ' kWh'
                    }
                }]
            }

        }, () => {
            const self = this;
            let interval =
                setInterval(() => {
                    let chart = self.refs.chart.getChart();
                    chart.reflow();
                    if (chart.series) {
                        let point = chart.series[0].points[0],
                            newVal,
                            inc = (Math.random() - 0.5) * 10;

                        newVal = parseFloat((point.y + inc).toLocaleString());
                        if (newVal < 0 || newVal > 16) {
                            newVal = point.y - inc;
                        }

                        point.update(newVal);
                        chart.reflow();
                    }
                }, 2000);
            this.setState({intervalId: interval});
        })
    }

    render() {
        return (
            this.state.config
                ? <ReactHighcharts config={this.state.config} ref="chart" />
            : null

        );
    }
}

RealtimeGaugeChart.propTypes = {
    title:PropTypes.string
};

export default RealtimeGaugeChart;
