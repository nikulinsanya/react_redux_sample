import React from 'react';
import PropTypes from 'prop-types';
import WidgetPreloader from 'components/WidgetPreloader';
import cx from 'classnames';

class LastDayProduction extends React.Component {
    render() {
        const { data } = this.props;
        if (!data) {
            return <WidgetPreloader/>
        }

        const { totalUsage, totalUsagePrev } = data;
        const totalP = totalUsage && (totalUsage.r281 + totalUsage.r282) || 0;
        const totalU = totalUsage && (totalUsage.r181 + totalUsage.r182) || 0;
        const prevU = totalUsagePrev && (totalUsagePrev.r281 + totalUsagePrev.r282) || 0;
        const trend = prevU && totalU ? (100 - (100 * prevU / totalU)).toLocaleString('nl-NL', { maximumFractionDigits: 1 }) : 0;
        const totalProdVsConsume = totalP && totalU ? (totalP / totalU * 100) : 0;
        const trendClass = cx({
            'panel panel-primary dashboard__widget': true,
            'red': trend < 0
        })

        return (
            <div className={trendClass}>
                <div className="panel-heading">
                    Electriciteitsteruglevering
                </div>
                <div className="panel-heading panel-heading--light">
                    <div className="row">
                        <div className="col-xs-5 left-icon">
                            <i className="fa fa-sun-o fa-5x"/>
                            <div>
                                {trend} %
                                {
                                    trend < 0
                                        ? <i className="fa fa-caret-down" aria-hidden="true"/>
                                        : <i className="fa fa-caret-up" aria-hidden="true"/>
                                }
                            </div>
                        </div>
                        <div className="col-xs-7 text-right">
                            <div className="big-number">
                                {totalP.toLocaleString('nl-NL', { maximumFractionDigits: 2 })}  kWh
                            </div>
                            <div className="big-number__sub">
                                Laag: {totalUsage && totalUsage.r281 && totalUsage.r281.toLocaleString('nl-NL', { maximumFractionDigits: 2 }) || 0}  kWh
                                <br/>
                                Hoog: {totalUsage && totalUsage.r282 && totalUsage.r282.toLocaleString('nl-NL', { maximumFractionDigits: 2 }) || 0} kWh
                            </div>
                        </div>
                    </div>
                </div>
                <div className="panel-footer">
                        <span className="pull-right">
                            {totalProdVsConsume.toLocaleString('nl-NL', { maximumFractionDigits: 1 })} % opgewekt van eigen gebruik
                        </span>
                    <div className="clearfix" />
                </div>
            </div>
        );
    }
}

LastDayProduction.propTypes = {
    data: PropTypes.object
};

export default LastDayProduction;
