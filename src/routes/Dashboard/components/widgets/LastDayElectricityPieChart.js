import React from 'react';
import PropTypes from 'prop-types';
import WidgetPreloader from 'components/WidgetPreloader';
var ReactHighcharts = require('react-highcharts');
import {CHART_COLORS} from 'constants'

class LastDayElectricityPieChart extends React.Component {
    constructor(props) {
        super(props);
        this.state = {

        };
    }

    prepareData(totalUsage, production) {
        const totalU = totalUsage && (totalUsage.r181 + totalUsage.r182) || 0;
        const totalP = totalUsage && (totalUsage.r281 + totalUsage.r282) || 0;
        return {
            chart: {
                type: 'pie',
                animation: ReactHighcharts.svg, // don't animate in old IE
                height: 130,
                backgroundColor: '#fff',
                margin: [0, 0, 0, 0],
                spacingTop: 0,
                spacingBottom: 0,
                spacingLeft: 0,
                spacingRight: 0
            },
            tooltip: {
                shared: true,
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            credits: {
                enabled: false
            },
            title: {
                text: ''
            },
            plotOptions:{
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    }
                }
            },
            series: [{
                name: 'percentage',
                data: [
                    { name: 'Consumptie laag',
                        y: Math.abs((totalUsage && totalUsage.r181 || 0)),
                        color: CHART_COLORS[0]},
                    {name: 'Consumptie hoog',
                        y: Math.abs((totalUsage && totalUsage.r182 || 0)),
                        color:  CHART_COLORS[1]},
                    {name: 'Teruglevering hoog',
                        y: Math.abs((totalUsage && totalUsage.r281 || 0)),
                        color: CHART_COLORS[10]},
                    {name: 'Teruglevering laag',
                        y: Math.abs((totalUsage && totalUsage.r282 || 0)),
                        color: CHART_COLORS[11]}
                ]
            }]
        }
    }

    render() {
        const { data } = this.props;
        if (!data) {
            return <WidgetPreloader/>
        }

        const { totalUsage, totalCost, totalUsagePrev } = data;
        const totalU = (totalUsage && (totalUsage.r181 + totalUsage.r182) || 0).toLocaleString('nl-NL', { maximumFractionDigits: 1 });
        const totalP = (totalUsage && (totalUsage.r281 + totalUsage.r282) || 0).toLocaleString('nl-NL', { maximumFractionDigits: 1 });
        const prevU = totalUsagePrev && (totalUsagePrev.r181 + totalUsagePrev.r182) || 0;
        const redelivery = totalP != 0 ? (' / ' + totalP + ' kWh') : '';
        const config = this.prepareData(totalUsage, totalP);
        return (
            config
            ? <div className="panel panel-primary dashboard__widget">
                    <div className="panel-heading">
                        Total inzicht
                    </div>
                    <div className="panel-heading chart-100">
                        <ReactHighcharts config={config} ref="chart" />
                    </div>
                    <div className="panel-footer">
                        <span className="pull-right">
                            {totalU} kWh
                            {redelivery}
                        </span>
                        <div className="clearfix" />
                    </div>
                </div>

            : null

        );
    }
}

LastDayElectricityPieChart.propTypes = {
    data: PropTypes.object
};

export default LastDayElectricityPieChart;
