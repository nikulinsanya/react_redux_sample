import React from 'react';
import PropTypes from 'prop-types';
import WidgetPreloader from 'components/WidgetPreloader';

class Weather extends React.Component {
    render() {
        const { data } = this.props;
        return (
            <div className="panel panel-primary dashboard__widget">
                <div className="panel-heading">
                    Weer & Label info
                </div>
                <div className="panel-heading panel-heading--light">
                    <div className="row">
                        <div className="col-xs-5 left-icon">
                            <img src="/img/partly_cloudy.png"/>
                            <div>
                                9°C
                            </div>
                        </div>
                        <div className="col-xs-7 text-right">
                            <div className="big-number">
                                <img src="/img/energy-labels/b.jpg" className="energy-label"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="panel-footer">
                        <span className="pull-right">
                            Voornamelijk bewolkt
                        </span>
                    <div className="clearfix" />
                </div>
            </div>
        );
    }
}

Weather.propTypes = {
    data: PropTypes.object
};

export default Weather;
