import React from 'react';
import PropTypes from 'prop-types';
var ReactHighcharts = require('react-highcharts');

class RealtimeLineChart extends React.Component {
    constructor(props) {
        super(props);
        this.state = {

        };
    }

    componentDidMount() {
        this.setState({
            config:{
                chart: {
                    type: 'spline',
                    animation: ReactHighcharts.svg, // don't animate in old IE
                    marginRight: 10,
                    height: 120,
                    backgroundColor: '#fff'
                },
                credits: {
                    enabled: false
                },
                title: {
                    text: this.props.title
                },
                xAxis: {
                    type: 'datetime',
                    tickPixelInterval: 150
                },
                yAxis: {
                    title: {
                        enabled: false
                    },
                    plotLines: [{
                        value: 0,
                        width: 1,
                        color: '#808080'
                    }]
                },
                legend: {
                    enabled: false
                },
                plotOptions: {
                    spline: {
                        marker: {
                            enabled: false
                        }
                    }
                },
                tooltip:{
                    pointFormat: '<span style="color:{point.color}">\u25CF</span> {series.name}: <b>{point.y:.3f} ' +
                    'kWh' + '</b><br/>'
                },
                series: [{
                    name: 'Electricity consumption',
                    data: (function () {
                        // generate an array of random data
                        var data = [],
                            time = (new Date()).getTime(),
                            i;

                        for (i = -19; i <= 0; i += 1) {
                            data.push({
                                x: time + i * 1000,
                                y: Math.abs((Math.random() * 10))
                            });
                        }
                        return data;
                    }())
                }]
            }
        }, () => {
            const self = this;
            let interval =
                setInterval(() => {
                    try {
                        let chart = self.refs.chart.getChart();
                        chart.reflow();
                        const x = (new Date()).getTime(), // current time
                            y = Math.abs(Math.random() * 10);
                        if (chart.series) {
                            chart.series[0].addPoint([x, y], true, true);
                            chart.reflow();
                        }
                    } catch (e) {
                        console.log('Some error, ', e);
                    }
                }, 2000);
            this.setState({intervalId: interval});
            let chart = self.refs.chart.getChart();
            chart.reflow();
        })
    }

    componentWillUnmount () {
        clearInterval(this.state.intervalId);
    }

    render() {
        return (
            this.state.config
            ? <div className="panel panel-primary dashboard__widget">
                    <div className="panel-heading">
                        Huidig elektriciteitsverbruik
                    </div>
                    <div className="panel-heading chart-100">
                        <ReactHighcharts config={this.state.config} ref="chart" />
                    </div>
                    <div className="panel-footer">
                        <span className="pull-right">
                            Electriciteits kosten van vandaag € 17.21
                        </span>
                        <div className="clearfix" />
                    </div>
                </div>
            : null

        );
    }
}

RealtimeLineChart.propTypes = {
    title: PropTypes.string
};

export default RealtimeLineChart;
