import React from 'react';
import PropTypes from 'prop-types';
import WidgetPreloader from 'components/WidgetPreloader';
import cx from 'classnames';

class LastDayDemand extends React.Component {
    render() {
        return (
            <div className="panel panel-primary dashboard__widget">
                <div className="panel-heading">
                    Demand response gisteren
                </div>
                <div className="panel-heading panel-heading--light">
                    <div className="row">
                        <div className="col-xs-4 left-icon">
                            <i className="fa fa-power-off fa-5x"/>
                            <div>
                                +3 events <i className="fa fa-caret-up" aria-hidden="true"/>
                            </div>
                        </div>
                        <div className="col-xs-8 text-right">
                            <div className="big-number">
                                7 nieuwe events
                            </div>
                            <div className="big-number__sub">
                                2 nieuwe restricties
                            </div>
                        </div>
                    </div>
                </div>
                <div className="panel-footer">
                        <span className="pull-right">
                            120 events afgelopen 30 dagen
                        </span>
                    <div className="clearfix" />
                </div>
            </div>
        );
    }
}

LastDayDemand.propTypes = {
    data: PropTypes.object,
    preloader: PropTypes.bool
};

export default LastDayDemand;
