import React from 'react';
import PropTypes from 'prop-types';
import WidgetPreloader from 'components/WidgetPreloader';

class LimitProgress extends React.Component {
    render() {
        const { data } = this.props;
        return (
            <div className="panel panel-primary dashboard__widget">
                <div className="panel-heading">
                    Maandbedrag
                </div>
                <div className="panel-heading panel-heading--light">
                    <div className="progress">
                        <div className="progress-value">
                            <div className="progress-value__label">
                                60%
                            </div>
                        </div>
                    </div>
                    <div className="big-number__sub">
                        € 62 van uw maand bedrag (€ 120)
                    </div>
                </div>
                <div className="panel-footer">
                        <span className="pull-right">
                            Onze inschatting: € 110
                        </span>
                    <div className="clearfix" />
                </div>
            </div>
        );
    }
}

LimitProgress.propTypes = {
    data: PropTypes.object
};

export default LimitProgress;
