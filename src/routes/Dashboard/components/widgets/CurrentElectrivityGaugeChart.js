import React from 'react';
import PropTypes from 'prop-types';
import ReactHighcharts from 'react-highcharts';
import SolidGauge from 'highcharts-solid-gauge';
import HighchartsMore from 'highcharts-more';

HighchartsMore(ReactHighcharts.Highcharts);
SolidGauge(ReactHighcharts.Highcharts);

class CurrentElectrivityGaugeChart extends React.Component {
    constructor(props) {
        super(props);
        this.state = {

        };
    }

    componentDidMount() {
        this.setState({
            config:{
                chart: {
                    type: 'solidgauge',
                    height: 130,
                    backgroundColor: '#fff',
                    margin: [0, 0, 0, 0],
                    spacingTop: 0,
                    spacingBottom: 0,
                    spacingLeft: 0,
                    spacingRight: 0
                },

                title: null,

                pane: {
                    center: ['50%', '70%'],
                    startAngle: -90,
                    endAngle: 90,
                    background: {
                        backgroundColor: '#fff',
                        innerRadius: '60%',
                        outerRadius: '100%',
                        shape: 'arc'
                    }
                },

                tooltip: {
                    enabled: false
                },
                credits: {
                    enabled: false
                },

                // the value axis
                yAxis: {
                    stops: [
                        [0.1, '#55BF3B'], // green
                        [0.5, '#DDDF0D'], // yellow
                        [0.9, '#DF5353'] // red
                    ],
                    lineWidth: 0,
                    minorTickInterval: null,
                    tickAmount: 2,
                    title: {
                        text: ''
                    },
                    labels: {
                        y: 14
                    },
                    min: 0,
                    max: 80
                },

                plotOptions: {
                    solidgauge: {
                        dataLabels: {
                            y: 22,
                            borderWidth: 0,
                            useHTML: true
                        }
                    }
                },
                series: [{
                    name: 'Electricity',
                    data: [68],
                    dataLabels: {
                        format: '<div style="text-align:center"><span style="font-size:14px;color:#006400;">' +
                        '{y}</span><br/>' +
                        '<span style="font-size:10px;color:#86af3f">kWh</span></div>'
                    },
                    tooltip: {
                        valueSuffix: ' kWh'
                    }
                }]
            }

        }, () => {
            let self = this;
            setTimeout(() => {
                try {
                    let chart = self.refs.chart.getChart();
                    chart.reflow();
                } catch (e) {}
            }, 400);
        })
    }

    componentWillUnmount () {
        clearInterval(this.state.intervalId);
    }

    render() {
        return (
            this.state.config
            ? <div className="panel panel-primary dashboard__widget">
                    <div className="panel-heading">
                        Vandaag electriciteitsverbruik
                    </div>
                    <div className="panel-heading chart-100">
                        <ReactHighcharts config={this.state.config} ref="chart" />
                    </div>
                    <div className="panel-footer">
                        <span className="pull-right">
                          84% al gebruikt
                        </span>
                        <div className="clearfix" />
                    </div>
                </div>

            : null

        );
    }
}

CurrentElectrivityGaugeChart.propTypes = {
    title: PropTypes.string
};

export default CurrentElectrivityGaugeChart;
