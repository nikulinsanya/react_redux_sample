import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { COMPONENTS } from 'constants';

class Index extends Component {
    componentWillMount() {
        route = '/dashboard';
        this.context.router.push(route);
        if (!this.props.role) {
            return;
        }

        const {componentPermissions} = this.props.role;
        const firstComponent = componentPermissions[0].component.id;
        let route;

        switch (firstComponent) {
        case COMPONENTS.DASHBOARD:
            route = '/dashboard';
            break;
        case COMPONENTS.DEVICES:
            route = '/devices';
            break;
        case COMPONENTS.TRAINEES:
            route = '/trainees';
            break;
        case COMPONENTS.HOTELS:
            route = '/hotels';
            break;
        default:
            route = '/trainees';
            break;
        }
        route = '/dashboard';
        this.context.router.push(route);
    }

    render() {
        return (
            <div/>
        );
    }
}

Index.propTypes = {
    role: PropTypes.object
};

Index.contextTypes = {
    router: PropTypes.object
};

export default Index;
