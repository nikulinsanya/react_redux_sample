import { connect } from 'react-redux';

import Index from '../components/Index';

const mapDispatchToProps = {};

const mapStateToProps = (state) => ({
    ...state.auth
});

export default connect(mapStateToProps, mapDispatchToProps)(Index);
