import Index from './containers/IndexContainer';

export default (store) => ({
    path: 'index',
    getComponent(nextState, cb) {
        cb(null, Index);
    }
});
