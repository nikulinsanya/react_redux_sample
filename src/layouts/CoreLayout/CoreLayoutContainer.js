import { connect } from 'react-redux';
import CoreLayout from './CoreLayout';
import {
    logout
} from 'store/auth/actions';
import {
    openSidebar,
    closeSidebar
} from 'store/ui/actions';

import {
    getUserProfile,
    selectAddress,
} from 'store/currentUser/actions';

import {
    showNotification,
    hideNotification
} from 'store/notifications/actions';

const mapDispatchProps = {
    logout,
    openSidebar,
    closeSidebar,
    getUserProfile,
    selectAddress,
    hideNotification,
    showNotification,
};

const mapStateToProps = (state) => ({
    ...state.auth,
    ...state.currentUser,
    ...state.notifications,
    ...state.ui
});

export default connect(mapStateToProps, mapDispatchProps)(CoreLayout);
