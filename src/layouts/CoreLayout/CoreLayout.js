import React, { Component } from 'react';
import Navigation from 'components/Navigation';
import 'styles/core.scss';
import PropTypes from 'prop-types';
import NotificationsHolder from 'components/NotificationsHolder';
import WidgetPreloader from 'components/WidgetPreloader';
import {getConfig} from 'constants';
import cx from 'classnames';
var ReactHighcharts = require('react-highcharts');
import moment from 'moment'
import 'moment/locale/nl'  // without this line it didn't work
moment.locale('nl')

class CoreLayout extends Component {

    componentDidMount() {
        this.props.getUserProfile();
        ReactHighcharts.Highcharts.setOptions({
            lang: {
                noData: 'Er zijn geen gegevens om weer te geven',
                loading: 'Wordt geladen...',
                months: ['januari', 'februari', 'maart', 'april', 'mei', 'juni', 'juli', 'augustus', 'september',
                    'oktober', 'november', 'december'],
                weekdays: ['zondag', 'maandag', 'dinsdag', 'woensdag', 'donderdag', 'vrijdag', 'zaterdag'],
                shortMonths: ['jan', 'feb', 'maa', 'apr', 'mei', 'jun', 'jul', 'aug', 'sep', 'okt', 'nov', 'dec'],
                exportButtonTitle: 'Exporteren',
                printButtonTitle: 'Printen',
                rangeSelectorFrom: 'Vanaf',
                rangeSelectorTo: 'Tot',
                rangeSelectorZoom: 'Periode',
                downloadPNG: 'Download als PNG',
                downloadJPEG: 'Download als JPEG',
                downloadPDF: 'Download als PDF',
                downloadSVG: 'Download als SVG',
                resetZoom: 'Reset',
                resetZoomTitle: 'Reset',
                thousandsSep: '.',
                decimalPoint: ','
            }
        });
    }

    changeFavicon(src) {
        var link = document.createElement('link'),
            oldLink = document.getElementById('dynamic-favicon');
        link.id = 'dynamic-favicon';
        link.rel = 'shortcut icon';
        link.href = src;
        if (oldLink) {
            document.head.removeChild(oldLink);
        }
        document.head.appendChild(link);
    }

    componentWillReceiveProps(nextProps, nextContext) {
        if (nextProps.user) {
            const adviserConfig = getConfig(nextProps.user && nextProps.user.adviserCode);
            this.changeFavicon('https://app.smartdodos.com/favicon.ico')
        }
    }

    render() {
        const {children} = this.props;
        const {
            logout,
            user,
            permissions,
            selectAddress,
            selectedAddress,
            notifications,
            hideNotification,
            showNotification,
            company
        } = this.props;

        if (!company || !permissions || !user) {
            return <WidgetPreloader/>;
        }
        const adviserConfig = getConfig(user && user.adviserCode);
        const mainContainerClass = cx({
            'b-wrapper': true,
            [`company-${adviserConfig && adviserConfig.mainClass}`]: true
        });

        return (
            <div className={mainContainerClass}>
                <Navigation
                    logout={logout}
                    user={user}
                    permissions={permissions}
                    selectAddress={selectAddress}
                    selectedAddress={selectedAddress}
                    company={company}
                />
                <NotificationsHolder
                    hideNotification={hideNotification}
                    notifications={notifications}/>

                <div id="page-wrapper">
                    {children}
                </div>
            </div>);
    }
}

CoreLayout.propTypes = {
    children: PropTypes.element.isRequired,
    getUserProfile: PropTypes.func.isRequired,
    logout: PropTypes.func.isRequired,
    user: PropTypes.object,
    permissions: PropTypes.object,
    selectedAddress: PropTypes.object,
    selectAddress: PropTypes.func.isRequired,
    notifications: PropTypes.array,
    hideNotification: PropTypes.func.isRequired,
    showNotification: PropTypes.func.isRequired,
    company: PropTypes.string
};

CoreLayout.contextTypes = {
    router: PropTypes.object.isRequired
};

export default CoreLayout;
