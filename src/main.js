import React from 'react';
import ReactDOM from 'react-dom';
import createStore from './store/createStore';
import AppContainer from './containers/AppContainer';
import {syncHistoryWithStore} from 'react-router-redux';
import {hashHistory} from 'react-router';
import Promise from 'promise-polyfill';
import 'whatwg-fetch';
import APIService from 'services/APIService';

if (!window.Promise) {
    window.Promise = Promise;
}

import PropTypes from 'prop-types'
import createClass from 'create-react-class'

Object.assign(React, {
    PropTypes,
    createClass
})

// ========================================================
// Store Instantiation
// ========================================================
const initialState = JSON.parse(localStorage.getItem(__LOCAL_STORAGE_KEY__)) || {};
const store = createStore(initialState);
const history = syncHistoryWithStore(hashHistory, store);

if (initialState && initialState.auth && initialState.auth) {
    APIService.setToken(initialState.auth.token);
}

// ========================================================
// Render Setup
// ========================================================
const MOUNT_NODE = document.getElementById('wrapper');

let render = () => {
    const routes = require('./routes/index').default(store);

    ReactDOM.render(
        <AppContainer store={store} routes={routes} history={history}/>,
        MOUNT_NODE
    );
};

// This code is excluded from production bundle
if (__LOCAL__) {
    if (module.hot) {
        // Development render functions
        const renderApp = render;
        const renderError = (error) => {
            const RedBox = require('redbox-react').default;

            ReactDOM.render(<RedBox error={error}/>, MOUNT_NODE);
        };

        // Wrap render in try/catch
        render = () => {
            try {
                renderApp();
            } catch (error) {
                console.error(error);
                renderError(error);
            }
        };

        // Setup hot module replacement
        module.hot.accept('./routes/index', () =>
            setImmediate(() => {
                ReactDOM.unmountComponentAtNode(MOUNT_NODE);
                render();
            })
        );
    }
}

// ========================================================
// Go!
// ========================================================
render();
