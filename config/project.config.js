/* eslint key-spacing:0 spaced-comment:0 */
const path = require('path');
const debug = require('debug')('app:config:project');
const argv = require('yargs').argv;

debug('Creating default configuration.');
// ========================================================
// Default Configuration
// ========================================================
const config = {
    env: process.env.NODE_ENV || 'local',

    // ----------------------------------
    // Project Structure
    // ----------------------------------
    path_base: path.resolve(__dirname, '..'),
    dir_client: 'src',
    dir_dist: 'dist',
    dir_public: 'public',
    dir_server: 'server',
    dir_test: 'tests',

    // ----------------------------------
    // Server Configuration
    // ----------------------------------
    server_host: 'localhost', // use string 'localhost' to prevent exposure on local network
    server_port: process.env.PORT || 3000,

    // ----------------------------------
    // Compiler Configuration
    // ----------------------------------
    compiler_babel: {
        cacheDirectory: true,
        plugins: ['transform-runtime'],
        presets: ['es2015', 'react', 'stage-0']
    },
    compiler_devtool: 'source-map',
    compiler_hash_type: 'hash',
    compiler_fail_on_warning: false,
    compiler_quiet: false,
    compiler_public_path: '/',
    compiler_stats: {
        chunks: false,
        chunkModules: false,
        colors: true
    },
    compiler_vendors: [
        'react',
        'react-redux',
        'react-router',
        'redux'
    ],

    // ----------------------------------
    // Test Configuration
    // ----------------------------------
    coverage_reporters: [
        {type: 'text-summary'},
        {type: 'lcov', dir: 'coverage'}
    ]
};

/************************************************
 -------------------------------------------------

 All Internal Configuration Below
 Edit at Your Own Risk

 -------------------------------------------------
 ************************************************/

// ------------------------------------
// Validate Vendor Dependencies
// ------------------------------------
const pkg = require('../package.json');

config.compiler_vendors = config.compiler_vendors
    .filter((dep) => {
        if (pkg.dependencies[dep]) return true;

        debug(
            `Package "${dep}" was not found as an npm dependency in package.json; ` +
            `it won't be included in the webpack vendor bundle.
       Consider removing it from \`compiler_vendors\` in ~/config/index.js`
        );
    });

// ------------------------------------
// Utilities
// ------------------------------------
function base() {
    const args = [config.path_base].concat([].slice.call(arguments));
    return path.resolve.apply(path, args);
}

config.paths = {
    base: base,
    client: base.bind(null, config.dir_client),
    public: base.bind(null, config.dir_public),
    dist: base.bind(null, config.dir_dist)
};

// ========================================================
// Environment Configuration
// ========================================================
debug(`Looking for environment overrides for NODE_ENV "${config.env}".`);
const environments = require('./environments.config');
const overrides = environments[config.env];
debug('Config env: ', config.env);
debug('Configuration: ', overrides);

if (overrides) {
    debug('Found overrides, applying to default configuration.');
    Object.assign(config, overrides(config));
} else {
    debug('No environment overrides found, defaults will be used.');
}

// ------------------------------------
// Environment
// ------------------------------------
// N.B.: globals added here must _also_ be added to .eslintrc
// nodeEnv in use by external libraries, and expected to be production if it's production build,
// like in our case this is for 'staging' and 'production'
const nodeEnv = config.env === 'staging' ? 'production' : config.env;

config.globals = {
    'process.env': {
        'NODE_ENV': JSON.stringify(nodeEnv)
    },
    'NODE_ENV': nodeEnv,
    '__LOCAL__': config.env === 'local',
    '__DEV__':  config.env === 'development',
    '__PROD__': config.env === 'staging' || config.env === 'production',
    '__TEST__': config.env === 'test',
    '__COVERAGE__': !argv.watch && config.env === 'test',
    '__BASENAME__': JSON.stringify(process.env.BASENAME || ''),
    '__API_URL__': JSON.stringify(config.api_url),
    '__ZABBIX_URL__': JSON.stringify(config.zabbix_url),
    '__P1_URL__': JSON.stringify(config.p1_url),
    '__COMPARISON_URL__': JSON.stringify(config.comparison_url),
    '__AUTH_URL__': JSON.stringify(config.auth_url),
    '__LOCAL_STORAGE_KEY__': JSON.stringify('_smartdodos_')
};

module.exports = config;
